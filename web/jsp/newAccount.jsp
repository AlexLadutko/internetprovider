<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customTags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<html>
<head>
    <title><fmt:message key="newAccount.title"/></title>
</head>
<body>
<%@include file="/jsp/header.jspf" %>
<h4 class="text-center"><fmt:message key="newAccount.header"/></h4>
<table class="hover" border="1">
    <thead>
    <tr>
        <th><fmt:message key="adminTariffList.tariffName"/></th>
        <th><fmt:message key="adminTariffList.payment"/></th>
        <th><fmt:message key="adminTariffList.speed"/></th>
        <th><fmt:message key="adminTariffList.traffic"/></th>
        <th><fmt:message key="newAccount.addAccount"/></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="tariff" items="${tariffs}">
        <tr>
            <td><c:out value="${ tariff.name }"/></td>
            <td><c:out value="${ tariff.monthPayment }"/></td>
            <td><c:out value="${ tariff.maxSpeed }"/></td>
            <td><c:out value="${ tariff.traffic }"/></td>
            <td>
                <form name="addAccountForm" method="POST" action="/controller">
                    <input type="hidden" name="command" value="add_account">
                    <input type="hidden" name="userId" value="${sessionScope.sessionUserId}"/>
                    <input type="hidden" name="tariffId" value=${tariff.tariffId}>
                    <input type="submit" value=<fmt:message key="newAccount.addAccount"/>>
                </form>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<ctg:footerTag/>
</body>
</html>