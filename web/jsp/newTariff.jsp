<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customTags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<html>
<head>
    <title><fmt:message key="newTariff.title"/></title>
</head>
<body>
<%@include file="/jsp/header.jspf" %>
${creationSuccessful}
${creationFailed}
<div class="row">
    <div class="medium-6 medium-centered large-4 large-centered columns">

        <form name="newTariffForm" method="POST" action="/controller">
            <input type="hidden" name="command" value="new_tariff"/>
            <div class="row column">
                <h4 class="text-center"><fmt:message key="newTariff.header"/></h4>
                ${invalidName}
                <label><fmt:message key="newTariff.name"/>
                    <input id="name" type="text" name="name" value="" required="true"/>
                </label>
                ${invalidPayment}
                <label><fmt:message key="newTariff.payment"/>
                    <input id="payment" type="text" name="payment" value="" required="true"/>
                </label>
                ${invalidSpeed}
                <label><fmt:message key="newTariff.speed"/>
                    <input id="speed" type="text" name="speed" value="" required="true"/>
                </label>
                ${invalidTraffic}
                <label><fmt:message key="newTariff.traffic"/>
                    <input id="traffic" type="text" name="traffic" value="" required="true"/>
                </label>

                <p><input type="submit" class="button expanded" value="<fmt:message key="newTariff.confirm"/>"/></p>

            </div>
        </form>

    </div>
</div>
<ctg:footerTag/>
</body>
</html>