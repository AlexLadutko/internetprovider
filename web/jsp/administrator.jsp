<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customTags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:bundle basename="properties.pagecontent">
    <html>
    <head>
        <title><fmt:message key="admin.title"/></title>
    </head>
    <body>
    <%@include file="/jsp/header.jspf" %>
    <c:if test="${sessionScope.role == 'ADMIN'}">
        <div class="inthis" style="text-align: center">
            <div class="block">
                <p class="text-center"><fmt:message key="admin.newTariff"/></p>

                <form name="forwardNewTariffForm" method="POST" action="/controller">
                    <input type="hidden" name="command" value="forward_new_tariff"/>

                    <p><input type="image" src="/img/addTariff.png" width="400" height="400"
                              alt="<fmt:message key="admin.newTariff"/>"></p>
                </form>

            </div>
            <div class="block">
                <p class="text-center"><fmt:message key="admin.tariffsList"/></p>

                <form name="tariffsListForm" method="POST" action="/controller">
                    <input type="hidden" name="command" value="admin_tariffs_list"/>

                    <p><input type="image" src="/img/tariffList.png" width="400" height="400"
                              alt="<fmt:message key="admin.tariffsList"/>"></p>
                </form>
            </div>
            <div class="block">
                <p class="text-center"><fmt:message key="admin.usersList"/></p>

                <form name="usersListForm" method="POST" action="/controller">
                    <input type="hidden" name="command" value="admin_users_list"/>

                    <p><input type="image" src="/img/userList.png" width="400" height="400"
                              alt="<fmt:message key="admin.usersList"/>"></p>
                </form>
            </div>
            <div class="block">
                <p class="text-center"><fmt:message key="admin.requestsList"/></p>

                <form name="requestsListForm" method="POST" action="/controller">
                    <input type="hidden" name="command" value="admin_requests_list"/>

                    <p><input type="image" src="/img/requestList.png" width="400" height="400"
                              alt="<fmt:message key="admin.requestsList"/>"></p>
                </form>
            </div>
        </div>
    </c:if>
    <ctg:footerTag/>
    </body>
    </html>
</fmt:bundle>