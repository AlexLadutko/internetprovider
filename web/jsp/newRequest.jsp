<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customTags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<html>
<head>
    <title><fmt:message key="newRequest.title"/></title>
</head>
<body>
<%@include file="/jsp/header.jspf" %>
<h4 class="text-center"><fmt:message key="newRequest.header"/></h4>
<table class="hover">
    <tr>
        <td><fmt:message key="newRequest.accountId"/></td>
        <td><c:out value="${ accountId }"/></td>
    </tr>
    <tr>
        <td><fmt:message key="newRequest.currentTariffName"/></td>
        <td><c:out value="${ currentTariff.name }"/></td>
    </tr>
    <tr>
        <td><fmt:message key="newRequest.currentTariffSpeed"/></td>
        <td><c:out value="${ currentTariff.maxSpeed }"/></td>
    </tr>
    <tr>
        <td><fmt:message key="newRequest.currentTariffPayment"/></td>
        <td><c:out value="${ currentTariff.monthPayment }"/></td>
    </tr>
    <tr>
        <td><fmt:message key="newRequest.currentTariffTraffic"/></td>
        <td><c:out value="${ currentTariff.traffic}"/></td>
    </tr>
</table>
<br/><br/><br/>

<table class="hover" border="1">
    <tr>
        <td><fmt:message key="adminTariffList.tariffName"/></td>
        <td><fmt:message key="adminTariffList.payment"/></td>
        <td><fmt:message key="adminTariffList.speed"/></td>
        <td><fmt:message key="adminTariffList.traffic"/></td>
        <td><fmt:message key="newRequest.addRequest"/></td>
    </tr>
    <c:forEach var="tariff" items="${tariffs}">
        <tr>
            <td><c:out value="${ tariff.name }"/></td>
            <td><c:out value="${ tariff.monthPayment }"/></td>
            <td><c:out value="${ tariff.maxSpeed }"/></td>
            <td><c:out value="${ tariff.traffic }"/></td>
            <td>
                <form name="chooseTariff" method="POST" action="/controller">
                    <input type="hidden" name="command" value="create_request">
                    <input type="hidden" name="accountId" value=${accountId}>
                    <input type="hidden" name="prevTariffId" value=${currentTariff.tariffId}>
                    <input type="hidden" name="newTariffId" value=${tariff.tariffId}>
                    <input type="submit" value=<fmt:message key="newRequest.addRequest"/>>
                </form>
            </td>
        </tr>
    </c:forEach>
</table>
<ctg:footerTag/>
</body>
</html>