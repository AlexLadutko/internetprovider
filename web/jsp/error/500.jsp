<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" isErrorPage="true" %>
    <html>
    <head>
        <title>Error 500 </title>
    </head>
    <body>
    <div class="text-center">
        <h4>
             ${pageContext.errorData.requestURI}<br/>
             ${pageContext.errorData.servletName}<br/>
             ${pageContext.errorData.statusCode}<br/>
             ${pageContext.errorData.throwable}
        </h4> <br/> <br/> <br/>
    </div>
    </body>
    </html>