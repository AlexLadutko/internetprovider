<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customTags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<html>
<head>
    <title><fmt:message key="changeUserPassword.title"/></title>
</head>
<body>
<%@include file="/jsp/header.jspf" %>
${changePasswordFailed}
<div class="row">
    <div class="medium-6 medium-centered large-4 large-centered columns">

        <form name="changeUserInfoForm" method="POST" action="/controller">
            <input type="hidden" name="command" value="change_user_password"/>
            <input type="hidden" name="userId" value="${sessionScope.sessionUserId}"/>

            <div class="row column">
                <h4 class="text-center"><fmt:message key="changeUserPassword.header"/></h4>
                ${invalidPassword}
                <label><fmt:message key="registration.password"/>
                    <input id="password" type="password" name="password" value="" required pattern=".{6,}"
                           title="<fmt:message key="registration.invalidPassword"/>"/>
                </label>
                ${invalidSecondPassword}
                <label><fmt:message key="registration.secondPassword"/>
                    <input id="secondPass" type="password" name="secondPassword" value="" required pattern=".{6,}"
                           title="<fmt:message key="registration.invalidPassword"/>"/>
                </label>

                <p><input type="submit" class="button expanded" value="<fmt:message key="changeUserPassword.change"/>"/>
                </p>

            </div>
        </form>
    </div>
</div>
<ctg:footerTag/>
</body>
</html>