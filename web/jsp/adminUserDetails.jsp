<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customTags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<html>
<head>
    <title><fmt:message key="adminUserDetails.title"/></title>
</head>
<body>
<%@include file="/jsp/header.jspf" %>
<h4 class="text-center"><fmt:message key="adminUserDetails.header"/></h4>
<table class="hover">
    <tr>
        <td><fmt:message key="adminUserDetails.id"/></td>
        <td><c:out value="${ user.userId }"/></td>
    </tr>
    <tr>
        <td><fmt:message key="adminUserDetails.login"/></td>
        <td><c:out value="${ user.login }"/></td>
    </tr>
    <tr>
        <td><fmt:message key="adminUserDetails.firstName"/></td>
        <td><c:out value="${ user.firstName }"/></td>
    </tr>
    <tr>
        <td><fmt:message key="adminUserDetails.lastName"/></td>
        <td><c:out value="${ user.lastName }"/></td>
    </tr>
    <tr>
        <td><fmt:message key="adminUserDetails.email"/></td>
        <td><c:out value="${ user.email}"/></td>
    </tr>
    <tr>
        <td><fmt:message key="adminUserDetails.phone"/></td>
        <td><c:out value="${ user.phone }"/></td>
    </tr>
    <tr>
        <td><fmt:message key="adminUserDetails.role"/></td>
        <td><c:out value="${ user.getRole().toString() }"/></td>
    </tr>
</table>
<br/><br/><br/>

<form name="userDetailsForm" method="POST" action="/controller">

    <input type="hidden" name="command" value="admin_user_details"/>
    <input type="hidden" name="userId" value=${user.userId}>

    <table border="1">
        <caption><fmt:message key="adminUserList.caption"/></caption>
        <thead>
        <tr>
            <th><fmt:message key="adminUserList.accountId"/></th>
            <th><fmt:message key="adminUserList.tariffId"/></th>
            <th><fmt:message key="adminUserList.balance"/></th>
            <th><fmt:message key="adminUserList.creationDate"/></th>
            <th><fmt:message key="adminUserList.ban"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="account" items="${accounts}">
            <tr>
                <td><c:out value="${ account.accountId }"/></td>
                <td><c:out value="${ account.tariffId }"/></td>
                <td><c:out value="${ account.balance }"/></td>
                <td><c:out value="${ account.creationDate }"/></td>
                <c:choose>
                    <c:when test="${ account.ban }">
                        <td>
                            <input checked type="checkbox" name="ban" value="${ account.accountId }">
                        </td>
                    </c:when>
                    <c:otherwise>
                        <td>
                            <input type="checkbox" name="ban" value="${ account.accountId }">
                        </td>
                    </c:otherwise>
                </c:choose>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <input type="submit" class="button float-center" value="<fmt:message key="adminUserList.confirm"/>"/>

</form>
<ctg:footerTag/>
</body>
</html>