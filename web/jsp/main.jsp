<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customTags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:bundle basename="properties.pagecontent">
    <html>
    <head>
        <link rel="stylesheet" href="/css/login.css">
        <title><fmt:message key="main.title"/></title>
    </head>
    <body class="login">
    <h2><fmt:message key="main.header"/></h2>

        <div class="fieldset">
            <div class="formHeader">
                <%@include file="/jsp/header.jspf" %>
            </div>
            <div class="login_form">
                <div class="fieldsetContents">
                    MAIN PAGE
                </div>
            </div>
        </div>
        <ctg:footerTag/>
    </body>
    </html>
</fmt:bundle>