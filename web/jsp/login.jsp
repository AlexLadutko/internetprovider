<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customTags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:bundle basename="properties.pagecontent">
    <html>
    <head>
        <title><fmt:message key="login.title"/></title>
    </head>
    <body>
    <%@include file="/jsp/header.jspf" %>
        ${registrationSuccessful}
        ${errorLoginPassMessage}
        ${wrongAction}
        ${nullPage}
    <div class="row">
        <div class="medium-6 medium-centered large-4 large-centered columns">

            <form name="loginForm" method="POST" action="/controller">
                <input type="hidden" name="command" value="login"/>
                <div class="row column log-in-form">
                    <h4 class="text-center"><fmt:message key="login.formheader"/></h4>
                    <label><fmt:message key="login.login"/>
                        <input id="login" type="text" name="login" value="" required="true"/>
                    </label>
                    <label><fmt:message key="login.password"/>
                        <input id="password" type="password" name="password" value="" required="true"/>
                    </label>

                    <p><input type="submit" class="button expanded" value="<fmt:message key="login.title"/>"/></p>

                </div>
            </form>

        </div>
    </div>
    <ctg:footerTag/>
    </body>
    </html>
</fmt:bundle>