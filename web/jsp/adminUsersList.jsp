<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customTags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<html>
<head>
    <title><fmt:message key="adminUserList.title"/></title>
</head>
<body>
<%@include file="/jsp/header.jspf" %>
${changeMessage}
<h4 class="text-center"><fmt:message key="adminUserList.header"/></h4>
<table class="hover" border="1">
    <thead>
    <tr>
        <th><fmt:message key="adminUserList.id"/></th>
        <th><fmt:message key="adminUserList.login"/></th>
        <th><fmt:message key="adminUserList.firstName"/></th>
        <th><fmt:message key="adminUserList.lastName"/></th>
        <th><fmt:message key="adminUserList.email"/></th>
        <th><fmt:message key="adminUserList.phone"/></th>
        <th><fmt:message key="adminUserList.role"/></th>
        <th><fmt:message key="adminUserList.banPretender"/></th>
        <th><fmt:message key="adminUserList.details"/></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="user" items="${users}">
        <tr>
            <td><c:out value="${ user.userId }"/></td>
            <td><c:out value="${ user.login }"/></td>
            <td><c:out value="${ user.firstName }"/></td>
            <td><c:out value="${ user.lastName }"/></td>
            <td><c:out value="${ user.email}"/></td>
            <td><c:out value="${ user.phone }"/></td>
            <td><c:out value="${ user.getRole().toString() }"/></td>
            <c:choose>
                <c:when test="${ banPretenders.contains(user.userId) }">
                    <td><c:out value="True"/></td>
                </c:when>
                <c:otherwise>
                    <td><c:out value="False"/></td>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${ user.getRole().toString() == 'ADMIN' }">
                    <td>
                        <form name="changeUser" method="POST" action="/controller">
                            <input type="hidden" name="command" value="forward_details_user">
                            <input type="hidden" name="userId" value=${user.userId}>
                            <input disabled type="submit" value=<fmt:message key="adminUserList.details"/>>
                        </form>
                    </td>
                </c:when>
                <c:otherwise>
                    <td>
                        <form name="changeUser" method="POST" action="/controller">
                            <input type="hidden" name="command" value="forward_details_user">
                            <input type="hidden" name="userId" value=${user.userId}>
                            <input type="submit" value=<fmt:message key="adminUserList.details"/>>
                        </form>
                    </td>
                </c:otherwise>
            </c:choose>
        </tr>
    </c:forEach>
    </tbody>
</table>
<ul class="pagination text-center" role="navigation" aria-label="Pagination">
    <c:if test="${currentPage != 1}">
        <li class="pagination-previous">
            <a href="/controller?command=admin_users_list&currentPage=${currentPage - 1}">
                <fmt:message key="adminUserList.previous"/>
            </a>
        </li>
    </c:if>

    <c:forEach begin="1" end="${numberOfPages}" var="counter">
        <c:choose>
            <c:when test="${currentPage eq counter}">
                <li class="current">${counter}</li>
            </c:when>
            <c:otherwise>
                <li>
                    <a href="/controller?command=admin_users_list&currentPage=${counter}">
                            ${counter}
                    </a>
                </li>
            </c:otherwise>
        </c:choose>
    </c:forEach>

    <c:if test="${currentPage lt numberOfPages}">
        <li class="pagination-next">
            <a href="/controller?command=admin_users_list&currentPage=${currentPage + 1}">
                <fmt:message key="adminUserList.next"/>
            </a>
        </li>
    </c:if>
</ul>
<ctg:footerTag/>
</body>
</html>