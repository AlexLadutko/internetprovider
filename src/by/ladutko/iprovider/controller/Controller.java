package by.ladutko.iprovider.controller;

import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.ActionFactory;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.ConfigurationManager;
import by.ladutko.iprovider.resource.MessageManager;
import by.ladutko.iprovider.pool.ConnectionPool;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//TODO: Look trough LogOutCommand

@WebServlet("/controller")
public class Controller extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String ERROR_PAGE = "/jsp/error/errorPage.jsp";

    public void destroy(){
        super.destroy();
        ConnectionPool.getInstance().closeAllConnections();
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String page;
            ActionFactory client = new ActionFactory();
            ActionCommand command = client.defineCommand(request);
            page = command.execute(request);
            if (page != null) {
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
                dispatcher.forward(request, response);
            } else {
                page = ConfigurationManager.getProperty("path.page.index");
                request.getSession().setAttribute("nullPage", MessageManager.getProperty("message.nullPage"));
                response.sendRedirect(request.getContextPath() + page);
            }
        }catch (CommandException e){
            LOGGER.error(e);
            String url = ERROR_PAGE;
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
            dispatcher.forward(request, response);
        }catch (Exception e){
            LOGGER.error(e);
            String url = ERROR_PAGE;
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
            dispatcher.forward(request, response);
        }
    }
}