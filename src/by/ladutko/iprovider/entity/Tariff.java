package by.ladutko.iprovider.entity;

public class Tariff implements Entity {
    private int tariffId;
    private String name;
    private boolean isDeprecated;
    private int monthPayment;
    private int maxSpeed;
    private int traffic;

    public int getTariffId() {
        return tariffId;
    }

    public void setTariffId(int tariffId) {
        this.tariffId = tariffId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDeprecated() {
        return isDeprecated;
    }

    public void setDeprecated(boolean deprecated) {
        isDeprecated = deprecated;
    }

    public int getMonthPayment() {
        return monthPayment;
    }

    public void setMonthPayment(int monthPayment) {
        this.monthPayment = monthPayment;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getTraffic() {
        return traffic;
    }

    public void setTraffic(int traffic) {
        this.traffic = traffic;
    }
}
