package by.ladutko.iprovider.entity;


import java.util.Date;

public class Request implements Entity {
    private int requestId;
    private int accountId;
    private int prevTariffId;
    private int newTariffId;
    private Date requestDate;
    private boolean isConfirmed;

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getPrevTariffId() {
        return prevTariffId;
    }

    public void setPrevTariffId(int prevTariffId) {
        this.prevTariffId = prevTariffId;
    }

    public int getNewTariffId() {
        return newTariffId;
    }

    public void setNewTariffId(int newTariffId) {
        this.newTariffId = newTariffId;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public boolean getConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }
}
