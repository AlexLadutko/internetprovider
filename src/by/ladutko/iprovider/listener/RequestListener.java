package by.ladutko.iprovider.listener;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebListener
public class RequestListener implements ServletRequestListener {

    private static final Logger LOGGER = LogManager.getLogger();

    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        String command = servletRequestEvent.getServletRequest().getParameter("command");
        HttpServletRequest request = (HttpServletRequest)servletRequestEvent.getServletRequest();
        LOGGER.info("Request initialized. Command: " + command + " " + request.getContextPath());
    }

    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
        String command = servletRequestEvent.getServletRequest().getParameter("command");
        LOGGER.info("Request destroyed. Command: " + command);
    }

}
