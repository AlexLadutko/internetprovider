package by.ladutko.iprovider.resource;

import java.util.ResourceBundle;
import java.util.Locale;

public class MessageManager {//TODO:remove singleton
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("properties.message", new Locale("", ""));

    public static void changeLocale(Locale locale) {
        resourceBundle = ResourceBundle.getBundle("properties.message", locale);
    }

    private MessageManager() { }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
