package by.ladutko.iprovider.resource;

import java.util.ResourceBundle;
public class ConfigurationManager {
    private final static ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("properties.config");
    private ConfigurationManager() { }
    public static String getProperty(String key) {
        return RESOURCE_BUNDLE.getString(key);
    }
}