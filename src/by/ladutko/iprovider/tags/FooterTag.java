package by.ladutko.iprovider.tags;


import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;


public class FooterTag extends TagSupport {

    @Override
    public int doStartTag() throws JspException {
        try {
            pageContext.getOut().write("<footer class=\"footer\">\n" +
                    "    <div class=\"container\">\n" +
                    "        <div class=\"row well\">\n" +
                    "            <div class=\"text-center\">\n" +"<hr>"+
                    "                <p>&copy; 2016 Alexey Ladutko<br></p>\n" +
                    "            </div>\n" +
                    "        </div>\n" +
                    "    </div>\n" +
                    "</footer>");
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}
