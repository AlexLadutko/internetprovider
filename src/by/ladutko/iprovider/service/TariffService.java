package by.ladutko.iprovider.service;

import by.ladutko.iprovider.dao.*;
import by.ladutko.iprovider.entity.*;
import by.ladutko.iprovider.resource.*;

import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.ArrayList;
import java.util.List;

//TODO: refactor with Optional;
//TODO:make an exception and exception throw through
//TODO: refactor data with properties with no hardcode
//TODO: make new tariff client validation
//TODO: make private

public class TariffService {
    private final static ResourceBundle REGEX = ResourceBundle.getBundle("properties.regex");
    private static TariffService instance = new TariffService();

    private TariffService() {

    }

    public static TariffService getInstance() {
        return instance;
    }

    public List<Tariff> buildUndeprecatedTariffList() throws ServiceException {
        try {
            return TariffDAO.getInstance().findAllUndeprecatedTariffs();
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public boolean validateName(String name) {
        if (name == null) {
            return false;
        }
        return name.matches(REGEX.getString("name"));

    }

    public boolean validatePayment(Integer payment) {
        if (payment == null) {
            return false;
        }
        return payment > 0;
    }

    public boolean validateSpeed(Integer speed) {
        if (speed == null) {
            return false;
        }
        return speed > 0;
    }

    public boolean validateTraffic(Integer traffic) {
        if (traffic == null) {
            return false;
        }
        return traffic >= -1;
    }

    public boolean insertTariff(Tariff tariff) throws ServiceException {
        try {
            TariffDAO tariffDAO = TariffDAO.getInstance();
            return tariffDAO.addTariff(tariff);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean updateTariff(Tariff tariff) throws ServiceException {
        try {
            TariffDAO tariffDAO = TariffDAO.getInstance();
            return tariffDAO.updateTariff(tariff);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public boolean isNameRegistered(String name) throws ServiceException {
        try {
            TariffDAO dao = TariffDAO.getInstance();
            Tariff tariff = dao.findByName(name);
            if (tariff != null) {
                return true;
            }
            return false;
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public HashMap<String, String> validateTariff(String name, Integer payment, Integer speed, Integer traffic)
            throws ServiceException {
        HashMap<String, String> errorMessages = new HashMap<>();
        if (validateName(name)) {
            if (isNameRegistered(name)) {
                errorMessages.put("invalidName", MessageManager.getProperty("message.presentTariff"));
            }
        } else {
            errorMessages.put("invalidName", MessageManager.getProperty("message.invalidTariffName"));
        }


        if (!(validatePayment(payment))) {
            errorMessages.put("invalidPayment", MessageManager.getProperty("message.invalidPayment"));
        }

        if (!(validateSpeed(speed))) {
            errorMessages.put("invalidSpeed", MessageManager.getProperty("message.invalidSpeed"));
        }

        if (!(validateTraffic(traffic))) {
            errorMessages.put("invalidTraffic", MessageManager.getProperty("message.invalidTraffic"));
        }

        return errorMessages;
    }

    public HashMap<String, String> validateTariffForUpdate(String name, Integer payment, Integer speed, Integer traffic)
            throws ServiceException {
        HashMap<String, String> errorMessages = new HashMap<>();
        if (!validateName(name)) {
            errorMessages.put("invalidName", MessageManager.getProperty("message.invalidTariffName"));
        }

        if (!(validatePayment(payment))) {
            errorMessages.put("invalidPayment", MessageManager.getProperty("message.invalidPayment"));
        }

        if (!(validateSpeed(speed))) {
            errorMessages.put("invalidSpeed", MessageManager.getProperty("message.invalidSpeed"));
        }

        if (!(validateTraffic(traffic))) {
            errorMessages.put("invalidTraffic", MessageManager.getProperty("message.invalidTraffic"));
        }

        return errorMessages;
    }

    //TODO: обработать входной null???
    public int buildTariffListDividedByPages(Integer pageOffset, Integer tariffsNumber, List<Tariff> returningList)
            throws ServiceException {
        try {
            return TariffDAO.getInstance().findAllTariffsDividedByPages(pageOffset, tariffsNumber, returningList);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public Tariff findTariffById(int id) throws ServiceException {
        try {
            return TariffDAO.getInstance().findById(id);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

}
