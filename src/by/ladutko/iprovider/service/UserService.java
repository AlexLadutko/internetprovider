package by.ladutko.iprovider.service;

import by.ladutko.iprovider.dao.*;
import by.ladutko.iprovider.entity.Account;
import by.ladutko.iprovider.entity.Tariff;
import org.apache.commons.codec.digest.DigestUtils;


import by.ladutko.iprovider.entity.User;
import by.ladutko.iprovider.resource.*;


import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;


//TODO: refactor with Optional;
//TODO:make an exception and exception throw through
//TODO: refactor data with properties with no hardcode
//TODO: сделать проверку на наличие таких логина и емэйла в системе при обновлении юзера

public class UserService {
    private final static ResourceBundle REGEX = ResourceBundle.getBundle("properties.regex");
    private static UserService instance = new UserService();

    private UserService() {

    }

    public static UserService getInstance() {
        return instance;
    }

    public boolean validateLogin(String login) {
        if (login == null) {
            return false;
        }
        return login.matches(REGEX.getString("login"));

    }

    public boolean validatePassword(String password) {
        if (password == null) {
            return false;
        }
        return password.matches(REGEX.getString("password"));

    }

    public boolean validateName(String name) {
        if (name == null) {
            return false;
        }
        return name.matches(REGEX.getString("name"));

    }

    public boolean validateEmail(String email) {
        if (email == null) {
            return false;
        }
        return email.matches(REGEX.getString("email"));

    }

    public boolean validatePhone(String phone) {
        if (phone == null) {
            return false;
        }
        return phone.matches(REGEX.getString("phone"));

    }

    public User checkLoginPassword(String login, String password) throws ServiceException {

        try {
            UserDAO dao = UserDAO.getInstance();
            User user = dao.findByLogin(login);
            if (user != null) {
                if (!user.getPassword().equals(DigestUtils.md5Hex(password))) {
                    user = null;
                }
            }
            return user;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean insertUser(User user) throws ServiceException {
        try {
            UserDAO dao = UserDAO.getInstance();
            return dao.addUser(user);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean updateUserInfo(User user) throws ServiceException {
        try {
            UserDAO dao = UserDAO.getInstance();
            return dao.updateUserInfo(user);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean updateUserPass(User user) throws ServiceException {
        try {
            UserDAO dao = UserDAO.getInstance();
            return dao.updateUserPass(user);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public boolean isUserRegistered(String login) throws ServiceException {
        try {
            UserDAO dao = UserDAO.getInstance();
            User user = dao.findByLogin(login);
            if (user != null) {
                return true;
            }
            return false;
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public boolean isLoginPresent(int id, String login) throws ServiceException {
        try {
            UserDAO dao = UserDAO.getInstance();
            User user = dao.findByLogin(login);
            if (user != null && user.getUserId() != id) {
                return true;
            }
            return false;
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public boolean isEmailRegistered(String email) throws ServiceException {
        try {
            UserDAO dao = UserDAO.getInstance();
            User user = dao.findByEmail(email);
            if (user != null) {
                return true;
            }
            return false;
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public boolean isEmailPresent(int id, String email) throws ServiceException {
        try {
            UserDAO dao = UserDAO.getInstance();
            User user = dao.findByEmail(email);
            if (user != null && user.getUserId() != id) {
                return true;
            }
            return false;
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public HashMap<String, String> validateUser(String login, String pass, String secondPass, String firstName,
                                                String lastName, String email, String phone) throws ServiceException {
        HashMap<String, String> errorMessages = new HashMap<>();

        if (validateLogin(login)) {
            if (isUserRegistered(login)) {
                errorMessages.put("invalidLogin", MessageManager.getProperty("message.presentLogin"));
            }
        } else {
            errorMessages.put("invalidLogin", MessageManager.getProperty("message.invalidLoginMessage"));
        }


        if (!(validatePassword(pass))) {
            errorMessages.put("invalidPassword", MessageManager.getProperty("message.invalidPasswordMessage"));
        }

        if (!(pass.equals(secondPass))) {
            errorMessages.put("invalidSecondPassword", MessageManager.getProperty("message.invalidSecondPasswordMessage"));
        }

        if (!(validateName(firstName))) {
            errorMessages.put("invalidFirstName", MessageManager.getProperty("message.invalidNameMessage"));
        }
        if (!(validateName(lastName))) {
            errorMessages.put("invalidLastName", MessageManager.getProperty("message.invalidNameMessage"));
        }
        if (!(validatePhone(phone))) {
            errorMessages.put("invalidPhone", MessageManager.getProperty("message.invalidPhoneMessage"));
        }
        if (validateEmail(email)) {
            if (isEmailRegistered(email)) {
                errorMessages.put("invalidEmail", MessageManager.getProperty("message.presentEmail"));
            }
        } else {
            errorMessages.put("invalidEmail", MessageManager.getProperty("message.invalidEmailMessage"));
        }
        return errorMessages;
    }

    public HashMap<String, String> validateUserForUpdate(int id, String login, String firstName,
                                                         String lastName, String email, String phone) throws ServiceException {
        HashMap<String, String> errorMessages = new HashMap<>();

        if (validateLogin(login)) {
            if (isLoginPresent(id, login)) {
                errorMessages.put("invalidLogin", MessageManager.getProperty("message.presentLogin"));
            }
        } else {
            errorMessages.put("invalidLogin", MessageManager.getProperty("message.invalidLoginMessage"));
        }

        if (!(validateName(firstName))) {
            errorMessages.put("invalidFirstName", MessageManager.getProperty("message.invalidNameMessage"));
        }
        if (!(validateName(lastName))) {
            errorMessages.put("invalidLastName", MessageManager.getProperty("message.invalidNameMessage"));
        }
        if (!(validatePhone(phone))) {
            errorMessages.put("invalidPhone", MessageManager.getProperty("message.invalidPhoneMessage"));
        }
        if (validateEmail(email)) {
            if (isEmailPresent(id, email)) {
                errorMessages.put("invalidEmail", MessageManager.getProperty("message.presentEmail"));
            }
        } else {
            errorMessages.put("invalidEmail", MessageManager.getProperty("message.invalidEmailMessage"));
        }
        return errorMessages;
    }

    public HashMap<String, String> validateUserForPassUpdate(String pass, String secondPass) {
        HashMap<String, String> errorMessages = new HashMap<>();

        if (!(validatePassword(pass))) {
            errorMessages.put("invalidPassword", MessageManager.getProperty("message.invalidPasswordMessage"));
        }

        if (!(pass.equals(secondPass))) {
            errorMessages.put("invalidSecondPassword", MessageManager.getProperty("message.invalidSecondPasswordMessage"));
        }
        return errorMessages;
    }

    public int buildUserListDividedByPages(Integer pageOffset, Integer tariffsNumber, List<User> returningList)
            throws ServiceException {
        try {
            return UserDAO.getInstance().findAllUsersDividedByPages(pageOffset, tariffsNumber, returningList);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public Set<Integer> buildBanPretendersSet() throws ServiceException {
        try {
            return UserDAO.getInstance().findBanPretenders();
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public User findUserById(int id) throws ServiceException {
        try {
            UserDAO dao = UserDAO.getInstance();
            User user = dao.findById(id);
            return user;
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public List<Account> buildUserAccountList(int id) throws ServiceException {
        try {
            return UserDAO.getInstance().findUserAccounts(id);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public boolean updateUserBans(int id, Set<String> banSet) throws ServiceException {
        try {
            List<Integer> accountList = UserDAO.getInstance().findAccountIdByUserId(id);
            return UserDAO.getInstance().updateUserBans(accountList, banSet);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public boolean addRequest(int accountId, int prevTariffId, int newTariffId) throws ServiceException {
        try {
            return UserDAO.getInstance().addRequest(accountId, prevTariffId, newTariffId);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public boolean addAccount(int userId, int tariffId) throws ServiceException {
        try {
            return UserDAO.getInstance().addAccount(userId, tariffId);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

    public boolean doEnroll(int accountId, int enrollSum) throws ServiceException {
        try {
            int newBalance = UserDAO.getInstance().findAccountBalanceById(accountId) + enrollSum;
            return UserDAO.getInstance().doEnroll(accountId, enrollSum, newBalance);
        }catch (DAOException e){
            throw new ServiceException(e);
        }
    }

}

