package by.ladutko.iprovider.service;

import by.ladutko.iprovider.dao.*;
import by.ladutko.iprovider.entity.*;

import java.util.List;

//TODO: refactor with Optional;
//TODO:make an exception and exception throw through
//TODO: refactor data with properties with no hardcode
//TODO: make new tariff client validation

public class RequestService {
    private static RequestService instance = new RequestService();

    private RequestService() {

    }

    public static RequestService getInstance() {
        return instance;
    }

    public int buildRequestsListDividedByPages(Integer pageOffset, Integer tariffsNumber, List<Request> returningList) throws ServiceException {
        try {
            return RequestDAO.getInstance().findUnconfirmedRequestsDividedByPages(pageOffset, tariffsNumber, returningList);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean confirmRequest(int requestID) throws ServiceException {
        try {
            return RequestDAO.getInstance().confirmRequest(requestID);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
