package by.ladutko.iprovider.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import by.ladutko.iprovider.entity.*;
import by.ladutko.iprovider.pool.*;

//TODO: refactor with Optional;
//TODO: refactor data with properties with no hardcode
//TODO: refactor encrypting
//TODO: returning and closing, ask about resource try()
//TODO: column account_id deleted, refactor it;
//TODO: refactor try with resources

//TODO:make an exception and exception throw through!!!!!!!!!!!!!!!!!!!!!!!!!

public class TariffDAO {
    private static final String INSERT_TARIFF_QUERY =
            "INSERT INTO tariffs(tariff_name, is_deprecated, month_payment, max_speed, traffic) values(?,0,?,?,?)";
    private static final String FIND_TARIFF_BY_ID_QUERY =
            "SELECT tariff_id, tariff_name, is_deprecated, month_payment, max_speed, traffic FROM tariffs WHERE tariff_id=?";
    private static final String FIND_TARIFF_BY_NAME_QUERY =
            "SELECT tariff_id, tariff_name, is_deprecated, month_payment, max_speed, traffic FROM tariffs WHERE tariff_name=?";
    private static final String FIND_DEVIDED_BY_PAGES_TARIFFS_QUERY =
            "SELECT SQL_CALC_FOUND_ROWS tariff_id, tariff_name, is_deprecated, month_payment, max_speed, traffic FROM tariffs limit ";
    private static final String NUMBER_OF_DIVIDED_TARIFFS_QUERY = "SELECT FOUND_ROWS()";
    private static final String UPDATE_TARIFF_QUERY =
            "UPDATE tariffs SET tariff_name=?, is_deprecated=?, month_payment=?,  max_speed=?, traffic=? WHERE tariff_id=?";
    private static final String FIND_UNDEPRECATED_TARIFFS_QUERY =
            "SELECT tariff_id, tariff_name, is_deprecated, month_payment, max_speed, traffic FROM tariffs WHERE is_deprecated=0";
    private static final TariffDAO INSTANCE = new TariffDAO();

    private TariffDAO() {

    }

    public static TariffDAO getInstance() {
        return INSTANCE;
    }

    public boolean addTariff(Tariff tariff) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        boolean success = false;
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_TARIFF_QUERY)) {
            statement.setString(1, tariff.getName());
            //statement.setBoolean(2, tariff.isDeprecated());
            statement.setInt(2, tariff.getMonthPayment());
            statement.setInt(3, tariff.getMaxSpeed());
            statement.setInt(4, tariff.getTraffic());
            statement.executeUpdate();
            success = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return success;
    }

    public boolean updateTariff(Tariff tariff)throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        boolean success = false;
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_TARIFF_QUERY)) {
            statement.setString(1, tariff.getName());
            statement.setBoolean(2, tariff.isDeprecated());
            statement.setInt(3, tariff.getMonthPayment());
            statement.setInt(4, tariff.getMaxSpeed());
            statement.setInt(5, tariff.getTraffic());
            statement.setInt(6, tariff.getTariffId());
            statement.executeUpdate();
            success = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return success;
    }

    public Tariff findById(int id) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_TARIFF_BY_ID_QUERY)) {
            statement.setInt(1, id);
            try (ResultSet set = statement.executeQuery()) {
                if (set.next()) {
                    Tariff tariff = new Tariff();

                    tariff.setTariffId(set.getInt(1));
                    tariff.setName(set.getString(2));
                    tariff.setDeprecated(set.getBoolean(3));
                    tariff.setMonthPayment(set.getInt(4));
                    tariff.setMaxSpeed(set.getInt(5));
                    tariff.setTraffic(set.getInt(6));
                    return tariff;
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return null;
    }

    public Tariff findByName(String name) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_TARIFF_BY_NAME_QUERY)) {
            statement.setString(1, name);
            try (ResultSet set = statement.executeQuery()) {
                if (set.next()) {
                    Tariff tariff = new Tariff();

                    tariff.setTariffId(set.getInt(1));
                    tariff.setName(set.getString(2));
                    tariff.setDeprecated(set.getBoolean(3));
                    tariff.setMonthPayment(set.getInt(4));
                    tariff.setMaxSpeed(set.getInt(5));
                    tariff.setTraffic(set.getInt(6));
                    return tariff;
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return null;
    }

    public List<Tariff> findAllUndeprecatedTariffs() throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<Tariff> resultList = new ArrayList<>();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_UNDEPRECATED_TARIFFS_QUERY)) {
            try (ResultSet set = statement.executeQuery()) {
                while (set.next()) {
                    Tariff tariff = new Tariff();

                    tariff.setTariffId(set.getInt(1));
                    tariff.setName(set.getString(2));
                    tariff.setDeprecated(set.getBoolean(3));
                    tariff.setMonthPayment(set.getInt(4));
                    tariff.setMaxSpeed(set.getInt(5));
                    tariff.setTraffic(set.getInt(6));
                    resultList.add(tariff);
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return resultList;
    }

    //TODO: rewrite it in human way (List in params, return Integer)
    public int findAllTariffsDividedByPages(Integer pageOffset, Integer tariffsNumber, List<Tariff> returningList)
            throws DAOException {
        int numberOfRecords = 0;
        Connection connection = ConnectionPool.getInstance().getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet set = statement.executeQuery(FIND_DEVIDED_BY_PAGES_TARIFFS_QUERY + pageOffset + ", " + tariffsNumber);
            while (set.next()) {
                Tariff tariff = new Tariff();

                tariff.setTariffId(set.getInt(1));
                tariff.setName(set.getString(2));
                tariff.setDeprecated(set.getBoolean(3));
                tariff.setMonthPayment(set.getInt(4));
                tariff.setMaxSpeed(set.getInt(5));
                tariff.setTraffic(set.getInt(6));
                returningList.add(tariff);
            }
            set = statement.executeQuery(NUMBER_OF_DIVIDED_TARIFFS_QUERY);
            if (set.next()) {
                numberOfRecords = set.getInt(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            ConnectionPool.getInstance().returnConnection((ProxyConnection) connection);
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
        return numberOfRecords;
    }

}
