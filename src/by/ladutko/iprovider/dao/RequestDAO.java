package by.ladutko.iprovider.dao;

import java.sql.*;
import java.text.DateFormat;
import java.util.*;

import by.ladutko.iprovider.entity.*;
import by.ladutko.iprovider.pool.*;

//TODO: refactor with Optional;
//TODO:make an exception and exception throw through
//TODO: refactor data with properties with no hardcode
//TODO: refactor encrypting
//TODO: returning and closing, ask about resource try()
//TODO: column account_id deleted, refactor it;
//TODO: refactor try with resources

public class RequestDAO {
    private static final String FIND_DIVIDED_BY_PAGES_UNCONFIRMED_REQUESTS_QUERY =
            "SELECT SQL_CALC_FOUND_ROWS request_id, account_id, prev_tf_id, new_tf_id, req_date, confirm FROM requests" +
                    " WHERE confirm = 0 limit ";
    private static final String NUMBER_OF_DIVIDED_REQUESTS_QUERY = "SELECT FOUND_ROWS()";
    private static final String FIND_REQUEST_BY_ID_QUERY =
            "SELECT request_id, account_id, prev_tf_id, new_tf_id, req_date, confirm FROM requests WHERE request_id=?";
    private static final String UPDATE_REQUEST_CONFIRM_BY_ID = "UPDATE requests SET confirm=1 WHERE request_id=?";
    private static final String UPDATE_ACCOUNT_TARIFF_BY_ID = "UPDATE accounts SET tariff_id=? WHERE account_id=?";
    private static final RequestDAO INSTANCE = new RequestDAO();

    private RequestDAO() {
    }

    public static RequestDAO getInstance() {
        return INSTANCE;
    }

    public int findUnconfirmedRequestsDividedByPages(Integer pageOffset, Integer tariffsNumber, List<Request> returningList)
            throws DAOException {
        int numberOfRecords = 0;
        Connection connection = ConnectionPool.getInstance().getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet set = statement.executeQuery(FIND_DIVIDED_BY_PAGES_UNCONFIRMED_REQUESTS_QUERY +
                    pageOffset + ", " + tariffsNumber);
            while (set.next()) {
                Request request = new Request();

                request.setRequestId(set.getInt(1));
                request.setAccountId(set.getInt(2));
                request.setPrevTariffId(set.getInt(3));
                request.setNewTariffId(set.getInt(4));
                request.setRequestDate(set.getDate(5));
                request.setConfirmed(set.getBoolean(6));

                returningList.add(request);
            }
            set = statement.executeQuery(NUMBER_OF_DIVIDED_REQUESTS_QUERY);
            if (set.next()) {
                numberOfRecords = set.getInt(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            ConnectionPool.getInstance().returnConnection((ProxyConnection) connection);
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
        return numberOfRecords;
    }

    public boolean confirmRequest(int requestId) throws DAOException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        boolean success = false;
        PreparedStatement statement = null;
        Request request = null;
        try {
            connection.setAutoCommit(false);
            statement = connection.prepareStatement(FIND_REQUEST_BY_ID_QUERY);
            statement.setInt(1, requestId);
            ResultSet set = statement.executeQuery();
            if (set.next()) {
                request = new Request();

                request.setRequestId(set.getInt(1));
                request.setAccountId(set.getInt(2));
                request.setPrevTariffId(set.getInt(3));
                request.setNewTariffId(set.getInt(4));
                request.setRequestDate(set.getDate(5));
                request.setConfirmed(set.getBoolean(6));
            } else {
                return false;
            }
            statement = connection.prepareStatement(UPDATE_REQUEST_CONFIRM_BY_ID);
            statement.setInt(1, requestId);
            statement.executeUpdate();

            statement = connection.prepareStatement(UPDATE_ACCOUNT_TARIFF_BY_ID);
            statement.setInt(1, request.getNewTariffId());
            statement.setInt(2, request.getAccountId());
            statement.executeUpdate();
            connection.commit();
            success = true;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new DAOException(e1);
            }
            throw new DAOException(e);
        } finally {
            try {
                connection.setAutoCommit(true);
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                throw new DAOException(e);
            } finally {
                ConnectionPool.getInstance().returnConnection((ProxyConnection) connection);
            }
        }
        return success;
    }
}
