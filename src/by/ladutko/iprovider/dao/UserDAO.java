package by.ladutko.iprovider.dao;

import java.sql.*;
import java.text.DateFormat;
import java.util.*;
import java.util.Date;
//TODO: refactor with Optional;
//TODO:make an exception and exception throw through
//TODO: refactor data with properties with no hardcode
//TODO: refactor encrypting
//TODO: returning and closing, ask about resource try()
//TODO: column account_id deleted, refactor it;
//TODO: refactor try with resources
import org.apache.commons.codec.digest.DigestUtils;

import by.ladutko.iprovider.entity.*;
import by.ladutko.iprovider.pool.*;

public class UserDAO {
    private static final String INSERT_USER_QUERY =
            "INSERT INTO users(login, password, first_name, last_name, email, phone, role) values(?,?,?,?,?,?,'USER')";
    private static final String FIND_USER_BY_ID_QUERY =
            "SELECT  login, password, first_name, last_name, email, phone, role FROM users WHERE user_id=?";
    private static final String FIND_USER_BY_LOGIN_QUERY =
            "SELECT user_id , login, password, first_name, last_name, email, phone, role FROM users WHERE login=?";
    private static final String FIND_USER_BY_EMAIL_QUERY =
            "SELECT user_id , login, password, first_name, last_name, email, phone, role FROM users WHERE email=?";
    private static final String FIND_DEVIDED_BY_PAGES_USERS_QUERY =
            "SELECT SQL_CALC_FOUND_ROWS user_id, login, password, first_name, last_name, email, phone, role FROM users limit ";
    private static final String FIND_BAN_PRETENDERS_QUERY = "SELECT user_id FROM accounts WHERE balance < 0 AND ban = 0";
    private static final String FIND_ACCOUNTS_BY_USER_QUERY =
            "SELECT account_id, user_id, tariff_id, balance, creation_date, ban FROM accounts WHERE user_id = ?";
    private static final String FIND_ACCOUNTS_ID_BY_USER_ID_QUERY =
            "SELECT account_id FROM accounts WHERE user_id = ?";
    private static final String UPDATE_BAN_BY_ACCOUNT_ID_QUERY =
            "UPDATE accounts SET ban=? WHERE account_id=?";
    private static final String UPDATE_USER_QUERY =
            "UPDATE users SET login = ?, first_name = ?, last_name = ?, email = ?, phone = ? WHERE user_id=?";
    private static final String UPDATE_PASSWORD_QUERY =
            "UPDATE users SET password = ? WHERE user_id=?";
    private static final String NUMBER_OF_DIVIDED_USERS_QUERY = "SELECT FOUND_ROWS()";
    private static final String INSERT_NEW_REQUEST_QUERY =
            "INSERT INTO requests(account_id, prev_tf_id, new_tf_id, req_date, confirm) values(?,?,?,NOW(),0)";
    private static final String INSERT_NEW_ACCOUNT_QUERY =
            "INSERT INTO accounts(user_id, tariff_id, balance, creation_date, ban) values(?,?,0,NOW(),0)";
    private static final String FIND_ACCOUNT_BALANCE_BY_ID_QUERY =
            "SELECT  balance FROM accounts WHERE account_id = ?";
    private static final String INSERT_NEW_TRANSACTION_QUERY =
            "INSERT INTO transactions(account_id, transaction_sum, transaction_type, transaction_date) values(?,?,'ENROLL',NOW())";
    private static final String UPDATE_ACCOUNT_BALANCE_QUERY =
            "UPDATE accounts SET balance = ? WHERE account_id=?";
    private static final UserDAO INSTANCE = new UserDAO();

    private UserDAO() {

    }

    public static UserDAO getInstance() {
        return INSTANCE;
    }

    public Integer findAccountBalanceById(int accountId) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_ACCOUNT_BALANCE_BY_ID_QUERY)) {
            statement.setInt(1, accountId);
            try (ResultSet set = statement.executeQuery()) {
                if (set.next()) {
                    Integer result = set.getInt(1);
                    return result;
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return null;
    }

    public boolean addRequest(int accountId, int prevTariffId, int newTariffId) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        boolean success = false;
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_NEW_REQUEST_QUERY)) {
            statement.setInt(1, accountId);
            statement.setInt(2, prevTariffId);
            statement.setInt(3, newTariffId);
            statement.executeUpdate();
            success = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return success;
    }

    public boolean addAccount(int userId, int tariffId) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        boolean success = false;
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_NEW_ACCOUNT_QUERY)) {
            statement.setInt(1, userId);
            statement.setInt(2, tariffId);
            statement.executeUpdate();
            success = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return success;
    }

    public boolean updateUserInfo(User user) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        boolean success = false;
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_USER_QUERY)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getFirstName());
            statement.setString(3, user.getLastName());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getPhone());
            statement.setInt(6, user.getUserId());
            statement.executeUpdate();
            success = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return success;
    }

    public boolean updateUserPass(User user) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        boolean success = false;
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_PASSWORD_QUERY)) {
            statement.setString(1, DigestUtils.md5Hex(user.getPassword()));
            statement.setInt(2, user.getUserId());
            statement.executeUpdate();
            success = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return success;
    }

    public boolean addUser(User user) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        boolean success = false;
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_USER_QUERY)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, DigestUtils.md5Hex(user.getPassword()));
            statement.setString(3, user.getFirstName());
            statement.setString(4, user.getLastName());
            statement.setString(5, user.getEmail());
            statement.setString(6, user.getPhone());
            statement.executeUpdate();
            success = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return success;
    }

    public User findById(int id) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_USER_BY_ID_QUERY)) {
            statement.setInt(1, id);
            try (ResultSet set = statement.executeQuery()) {
                if (set.next()) {
                    User user = new User();

                    user.setUserId(id);
                    user.setLogin(set.getString(1));
                    user.setPassword(set.getString(2));
                    user.setFirstName(set.getString(3));
                    user.setLastName(set.getString(4));
                    user.setEmail(set.getString(5));
                    user.setPhone(set.getString(6));
                    String strRole = set.getString(7).toUpperCase();
                    user.setRole(User.Role.valueOf(strRole));
                    return user;
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return null;
    }

    public User findByLogin(String login) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_USER_BY_LOGIN_QUERY)) {
            statement.setString(1, login);
            try (ResultSet set = statement.executeQuery()) {
                if (set.next()) {
                    User user = new User();

                    user.setUserId(Integer.parseInt(set.getString(1)));
                    user.setLogin(set.getString(2));
                    user.setPassword(set.getString(3));
                    user.setFirstName(set.getString(4));
                    user.setLastName(set.getString(5));
                    user.setEmail(set.getString(6));
                    user.setPhone(set.getString(7));
                    String strRole = set.getString(8).toUpperCase();
                    user.setRole(User.Role.valueOf(strRole));

                    return user;
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return null;
    }

    public User findByEmail(String email) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_USER_BY_EMAIL_QUERY)) {
            statement.setString(1, email);
            try (ResultSet set = statement.executeQuery()) {
                if (set.next()) {
                    User user = new User();

                    user.setUserId(Integer.parseInt(set.getString(1)));
                    user.setLogin(set.getString(2));
                    user.setPassword(set.getString(3));
                    user.setFirstName(set.getString(4));
                    user.setLastName(set.getString(5));
                    user.setEmail(set.getString(6));
                    user.setPhone(set.getString(7));
                    String strRole = set.getString(8).toUpperCase();
                    user.setRole(User.Role.valueOf(strRole));
                    return user;
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return null;
    }

    public int findAllUsersDividedByPages(Integer pageOffset, Integer tariffsNumber, List<User> returningList)
            throws DAOException {
        int numberOfRecords = 0;
        Connection connection = ConnectionPool.getInstance().getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet set = statement.executeQuery(FIND_DEVIDED_BY_PAGES_USERS_QUERY + pageOffset + ", " + tariffsNumber);
            while (set.next()) {
                User user = new User();

                user.setUserId(set.getInt(1));
                user.setLogin(set.getString(2));
                user.setPassword(set.getString(3));
                user.setFirstName(set.getString(4));
                user.setLastName(set.getString(5));
                user.setEmail(set.getString(6));
                user.setPhone(set.getString(7));
                String strRole = set.getString(8).toUpperCase();
                user.setRole(User.Role.valueOf(strRole));
                returningList.add(user);
            }
            set = statement.executeQuery(NUMBER_OF_DIVIDED_USERS_QUERY);
            if (set.next()) {
                numberOfRecords = set.getInt(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            ConnectionPool.getInstance().returnConnection((ProxyConnection) connection);
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
        return numberOfRecords;
    }

    public Set<Integer> findBanPretenders() throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Set<Integer> resultSet = new HashSet<Integer>();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_BAN_PRETENDERS_QUERY)) {
            try (ResultSet set = statement.executeQuery()) {
                while (set.next()) {
                    resultSet.add(set.getInt(1));
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return resultSet;
    }

    public List<Account> findUserAccounts(int id) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<Account> resultList = new ArrayList<Account>();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_ACCOUNTS_BY_USER_QUERY)) {
            statement.setInt(1, id);
            try (ResultSet set = statement.executeQuery()) {
                while (set.next()) {
                    Account account = new Account();

                    account.setAccountId(set.getInt(1));
                    account.setUserId(set.getInt(2));
                    account.setTariffId(set.getInt(3));
                    account.setBalance(set.getInt(4));
                    account.setCreationDate(set.getDate(5));
                    account.setBan(set.getBoolean(6));

                    resultList.add(account);
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return resultList;
    }

    public List<Integer> findAccountIdByUserId(int id) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<Integer> resultList = new ArrayList<Integer>();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_ACCOUNTS_ID_BY_USER_ID_QUERY)) {
            statement.setInt(1, id);
            try (ResultSet set = statement.executeQuery()) {
                while (set.next()) {
                    resultList.add(set.getInt(1));
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return resultList;
    }

    public boolean updateUserBans(List<Integer> accounts, Set<String> bannedAccounts) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        boolean success = false;
        try (Connection connection = pool.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_BAN_BY_ACCOUNT_ID_QUERY)) {
            for (Integer account : accounts) {
                if (bannedAccounts.contains(account.toString())) {
                    statement.setBoolean(1, true);
                    statement.setInt(2, account);
                } else {
                    statement.setBoolean(1, false);
                    statement.setInt(2, account);
                }
                statement.executeUpdate();
            }
            success = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return success;
    }

    public boolean doEnroll(int accountId, int enrollSum, int newBalance) throws DAOException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        boolean success = false;
        PreparedStatement statement = null;
        try {
            connection.setAutoCommit(false);

            statement = connection.prepareStatement(INSERT_NEW_TRANSACTION_QUERY);
            statement.setInt(1, accountId);
            statement.setInt(2, enrollSum);
            statement.executeUpdate();

            statement = connection.prepareStatement(UPDATE_ACCOUNT_BALANCE_QUERY);
            statement.setInt(1, newBalance);
            statement.setInt(2, accountId);
            statement.executeUpdate();
            connection.commit();
            success = true;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new DAOException(e1);
            }
            throw new DAOException(e);
        } finally {
            try {
                connection.setAutoCommit(true);
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                throw new DAOException(e);
            }
            ConnectionPool.getInstance().returnConnection((ProxyConnection) connection);

        }
        return success;
    }
}

