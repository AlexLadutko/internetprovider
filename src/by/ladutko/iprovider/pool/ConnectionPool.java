package by.ladutko.iprovider.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//TODO: make own exception and use logger
//TODO: refactor while into for, where marked
public class ConnectionPool {
    private static final Logger LOGGER = LogManager.getLogger();
    private final static ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("properties.database");
    private static ConnectionPool instance;
    private static AtomicBoolean isInitialized = new AtomicBoolean();
    private static Lock lock = new ReentrantLock();
    private ArrayBlockingQueue<ProxyConnection> connections;

    //TODO: try-finally
    public static ConnectionPool getInstance(){
        if (!isInitialized.get()){
            lock.lock();
            if (instance == null){
                instance = new ConnectionPool();
                isInitialized.getAndSet(true);
            }
            lock.unlock();
        }
        return instance;
    }
    //TODO: exceptions
    private ConnectionPool(){
        Properties prop = new Properties();
        prop.put("user",RESOURCE_BUNDLE.getString("db.user"));
        prop.put("password",RESOURCE_BUNDLE.getString("db.password"));
        prop.put("autoReconnect",RESOURCE_BUNDLE.getString("db.autoReconnect"));
        prop.put("characterEncoding", RESOURCE_BUNDLE.getString("db.encoding"));
        prop.put("useUnicode",RESOURCE_BUNDLE.getString("db.useUnicode"));
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException e) {
            throw new RuntimeException();
        }
        connections = new ArrayBlockingQueue<>(Integer.valueOf(RESOURCE_BUNDLE.getString("db.poolSize")));
        for (int i = 0; i < Integer.valueOf(RESOURCE_BUNDLE.getString("db.poolSize")); ++ i){
            try {
                Connection connection = DriverManager.getConnection(RESOURCE_BUNDLE.getString("db.url"),prop);
                ProxyConnection proxy = new ProxyConnection(connection);
                connections.add(proxy);
            } catch (SQLException e) {
                throw new RuntimeException();
            }
        }

    }

    public Connection getConnection(){
        ProxyConnection connection = null;
        try {
            connection = connections.take();
        } catch (InterruptedException e) {
            LOGGER.error("Exception in getConnection method");
        }
        return connection;
    }

    public void returnConnection(ProxyConnection connection){
        try {
            connections.put(connection);
        } catch (InterruptedException e) {
            LOGGER.error("Exception in returnConnection method");
        }
    }

    public void closeAllConnections(){
        while (!connections.isEmpty()){ ///FOR
            try {
                ProxyConnection connection = connections.take();
                connection.realClose();
            } catch (SQLException e) {
                LOGGER.error("ConnectionPool can't close connection");
            } catch (InterruptedException e) {
                LOGGER.error("Exception in returnConnection method");
            }
        }
    }
}

