package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.ServiceException;
import by.ladutko.iprovider.service.UserService;
import by.ladutko.iprovider.entity.User;

public class ChangeUserPasswordCommand implements ActionCommand {
    private static final String USER_ID = "userId";
    private static final String PASSWORD = "password";
    private static final String SECOND_PASSWORD = "secondPassword";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try {
            String page = null;
            int id = Integer.parseInt(request.getParameter(USER_ID));
            String pass = request.getParameter(PASSWORD);
            String passTwo = request.getParameter(SECOND_PASSWORD);

            UserService service = UserService.getInstance();

            HashMap<String, String> errorMessages = service.validateUserForPassUpdate(pass, passTwo);
            User user = new User();
            user.setUserId(id);
            user.setPassword(pass);
            if (errorMessages.isEmpty()) {

                if (service.updateUserPass(user)) {
                    request.setAttribute("changePasswordSuccessful", MessageManager.getProperty("message.changeUserSuccessful"));
                    page = ConfigurationManager.getProperty("path.page.returnToUserCabinet")+id;
                } else {
                    request.setAttribute("changePasswordFailed", MessageManager.getProperty("message.changeUserFailed"));
                    request.setAttribute("user", user);
                    page = ConfigurationManager.getProperty("path.page.changeUserPassword");
                }
            } else {
                for (Map.Entry<String, String> entry : errorMessages.entrySet()) {
                    request.setAttribute(entry.getKey(), entry.getValue());
                }
                request.setAttribute("user", user);
                page = ConfigurationManager.getProperty("path.page.changeUserPassword");
            }
            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }
    }
}
