package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.ServiceException;
import by.ladutko.iprovider.service.UserService;
import by.ladutko.iprovider.entity.User;

//TODO: refactor with Optional;
//TODO:make an exception and exception throw through
//TODO: refactor data with properties with no hardcode
//TODO: refactor encrypting
//TODO: refactor role
public class LogInCommand implements ActionCommand {

    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";

    public String execute(HttpServletRequest request) throws CommandException {
        try {
            String page = null;
            String login = request.getParameter(PARAM_NAME_LOGIN);
            String pass = request.getParameter(PARAM_NAME_PASSWORD);
            UserService service = UserService.getInstance();
            User user = service.checkLoginPassword(login,pass);
            if (user != null) {
                request.getSession().setAttribute("user", user.getFirstName());
                request.getSession().setAttribute("role", user.getRole());
                request.getSession().setAttribute("sessionUserId", user.getUserId());
                if ("USER".equals(user.getRole().toString())){
                page = ConfigurationManager.getProperty("path.page.returnToUserCabinet")+user.getUserId();
                }
                if ("ADMIN".equals(user.getRole().toString())){
                    page = ConfigurationManager.getProperty("path.page.administrator");
                }
            } else {
                request.setAttribute("errorLoginPassMessage", MessageManager.getProperty("message.loginError"));
                page = ConfigurationManager.getProperty("path.page.login");
            }

            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }
    }
}
