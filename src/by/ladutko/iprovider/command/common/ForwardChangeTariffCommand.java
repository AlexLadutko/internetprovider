package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.*;
import by.ladutko.iprovider.entity.*;

//TODO:TEST!!!

public class ForwardChangeTariffCommand implements ActionCommand {

    private static final String PARAM_TARIFF_ID = "tariffId";

    public String execute(HttpServletRequest request) throws CommandException {
        try {
            int tariffId = Integer.valueOf(request.getParameter(PARAM_TARIFF_ID));
            Tariff tariff = TariffService.getInstance().findTariffById(tariffId);
            request.setAttribute("tariff", tariff );
            String page = ConfigurationManager.getProperty("path.page.changeTariff");
            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }
    }
}
