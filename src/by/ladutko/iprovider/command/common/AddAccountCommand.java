package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.*;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.*;
import by.ladutko.iprovider.entity.*;

//TODO:TEST hard!!!


public class AddAccountCommand implements ActionCommand {
    private static final String USER_ID = "sessionUserId";
    private static final String TARIFF_ID = "tariffId";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try {
            String page = null;
            int userId = (Integer)request.getSession().getAttribute(USER_ID);
            int tariffId = Integer.valueOf(request.getParameter(TARIFF_ID ));

            UserService service = UserService.getInstance();

            if (service.addAccount(userId, tariffId)) {
                request.setAttribute("addAccountMessage", MessageManager.getProperty("message.changeUserSuccessful"));
                page = ConfigurationManager.getProperty("path.page.returnToUserCabinet") + userId;
            } else {
                request.setAttribute("addAccountMessage", MessageManager.getProperty("message.changeUserFailed"));
                page = ConfigurationManager.getProperty("path.page.returnToUserCabinet") + userId;
            }
            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }
    }
}
