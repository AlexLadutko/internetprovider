package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.*;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.*;
import by.ladutko.iprovider.entity.*;

//TODO:TEST hard!!!
//TODO:что делать с запросом от того же аккаунта?

public class CreateRequestCommand implements ActionCommand {
    private static final String USER_ID = "sessionUserId";
    private static final String ACCOUNT_ID = "accountId";
    private static final String PREV_TARIFF_ID = "prevTariffId";
    private static final String NEW_TARIFF_ID = "newTariffId";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try {
            String page = null;
            int userId = (Integer)request.getSession().getAttribute(USER_ID);
            int accountId = Integer.valueOf(request.getParameter(ACCOUNT_ID));
            int prevTariffId = Integer.valueOf(request.getParameter(PREV_TARIFF_ID ));
            int newTariffId = Integer.valueOf(request.getParameter(NEW_TARIFF_ID));

            UserService service = UserService.getInstance();

            if (service.addRequest(accountId, prevTariffId, newTariffId)) {
                request.setAttribute("createRequestSuccessful", MessageManager.getProperty("message.createRequestSuccessful"));
                page = ConfigurationManager.getProperty("path.page.returnToUserCabinet") + userId;
            } else {
                request.setAttribute("createRequestFailed", MessageManager.getProperty("message.createRequestFailed"));
                page = ConfigurationManager.getProperty("path.page.returnToUserCabinet") + userId;
            }
            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }
    }
}
