package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.*;
import by.ladutko.iprovider.entity.*;


//TODO: strongly refactor it, look through jsp and refactor names in jsp
public class AdminTariffsListCommand implements ActionCommand {
    private static final int ROWS_IN_PAGE = 3;
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try {
            String page = null;
            int currentPage = 1;
            if (request.getParameter("currentPage") != null) {
                currentPage = Integer.parseInt(request.getParameter("currentPage"));
            }
            TariffService service = TariffService.getInstance();
            List<Tariff> tariffs = new ArrayList<Tariff>();
            int numberOfRecords = service.buildTariffListDividedByPages((currentPage - 1) * ROWS_IN_PAGE, ROWS_IN_PAGE, tariffs);

            int numberOfPages = (int) Math.ceil(numberOfRecords * 1.0 / ROWS_IN_PAGE);
            request.setAttribute("tariffs", tariffs);
            request.setAttribute("numberOfPages", numberOfPages);
            request.setAttribute("currentPage", currentPage);
            page = ConfigurationManager.getProperty("path.page.adminTariffsList");

            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }
    }
}
