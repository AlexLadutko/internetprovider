package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.ServiceException;
import by.ladutko.iprovider.service.UserService;
import by.ladutko.iprovider.entity.User;

public class ChangeUserInfoCommand implements ActionCommand {
    private static final String USER_ID = "userId";
    private static final String LOGIN = "login";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String EMAIL = "email";
    private static final String PHONE = "phone";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try {
            String page = null;
            int id = Integer.parseInt(request.getParameter(USER_ID));
            String login = request.getParameter(LOGIN);
            String firstName = request.getParameter(FIRST_NAME);
            String lastName = request.getParameter(LAST_NAME);
            String email = request.getParameter(EMAIL);
            String phone = request.getParameter(PHONE);

            UserService service = UserService.getInstance();

            HashMap<String, String> errorMessages = service.validateUserForUpdate(id, login, firstName, lastName, email, phone);
            User user = new User();
            user.setUserId(id);
            user.setLogin(login);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmail(email);
            user.setPhone(phone);
            if (errorMessages.isEmpty()) {

                if (service.updateUserInfo(user)) {
                    request.setAttribute("changeUserSuccessful", MessageManager.getProperty("message.changeUserSuccessful"));
                    page = ConfigurationManager.getProperty("path.page.returnToUserCabinet") + id;
                } else {
                    request.setAttribute("changeUserFailed", MessageManager.getProperty("message.changeUserFailed"));
                    request.setAttribute("user", user);
                    page = ConfigurationManager.getProperty("path.page.changeUserInfo");
                }
            } else {
                for (Map.Entry<String, String> entry : errorMessages.entrySet()) {
                    request.setAttribute(entry.getKey(), entry.getValue());
                }
                request.setAttribute("user", user);
                page = ConfigurationManager.getProperty("path.page.changeUserInfo");
            }
            return page;
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
    }
}
