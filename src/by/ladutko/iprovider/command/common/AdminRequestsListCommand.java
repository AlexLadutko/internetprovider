package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.*;
import by.ladutko.iprovider.entity.*;
import by.ladutko.iprovider.command.*;

//TODO:TEST!!!

public class AdminRequestsListCommand implements ActionCommand {

    private static final int ROWS_IN_PAGE = 5;

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try {
            String page = null;
            int currentPage = 1;
            if (request.getParameter("currentPage") != null) {
                currentPage = Integer.parseInt(request.getParameter("currentPage"));
            }
            RequestService service = RequestService.getInstance();
            List<Request> requests = new ArrayList<Request>();
            int numberOfRecords = service.buildRequestsListDividedByPages((currentPage - 1) * ROWS_IN_PAGE, ROWS_IN_PAGE, requests);

            int numberOfPages = (int) Math.ceil(numberOfRecords * 1.0 / ROWS_IN_PAGE);
            request.setAttribute("requests", requests);
            request.setAttribute("numberOfPages", numberOfPages);
            request.setAttribute("currentPage", currentPage);
            page = ConfigurationManager.getProperty("path.page.adminRequestsList");
            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }
    }
}
