package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.*;
import by.ladutko.iprovider.entity.*;

//TODO:TEST!!!

public class ForwardAddAccountCommand implements ActionCommand {
    private static final String USER_ID = "userId";

    public String execute(HttpServletRequest request) throws CommandException {
        try {
            String page = null;

            int userId = Integer.valueOf(request.getParameter(USER_ID));

            TariffService service = TariffService.getInstance();
            List<Tariff> tariffs = service.buildUndeprecatedTariffList();

            request.setAttribute("userId", userId);
            request.setAttribute("tariffs", tariffs);
            page = ConfigurationManager.getProperty("path.page.newAccount");

            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }
    }
}
