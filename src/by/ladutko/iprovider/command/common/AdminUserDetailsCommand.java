package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.*;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.*;
import by.ladutko.iprovider.entity.*;

//TODO:TEST!!!

public class AdminUserDetailsCommand implements ActionCommand {
    private static final String USER_ID = "userId";
    private static final String BAN = "ban";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try {
            String page = null;
            Integer userId = Integer.parseInt(request.getParameter(USER_ID));
            Set<String> banSet;
            if (request.getParameterValues(BAN) == null) {
                banSet = new HashSet<String>();
            } else {
                banSet = new HashSet<String>(Arrays.asList(request.getParameterValues(BAN)));
            }
            UserService service = UserService.getInstance();
            if (service.updateUserBans(userId, banSet)) {
                request.setAttribute("changeMessage", MessageManager.getProperty("message.userBanUpdateSuccessful"));
                page = ConfigurationManager.getProperty("path.page.returnToAdminUsersList");
            } else {
                request.setAttribute("changeMessage", MessageManager.getProperty("message.userBanUpdateFailed"));
                page = ConfigurationManager.getProperty("path.page.returnToAdminUsersList");
            }

            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }
    }
}
