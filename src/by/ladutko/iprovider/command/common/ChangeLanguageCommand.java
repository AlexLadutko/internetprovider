package by.ladutko.iprovider.command.common;

import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;
public class ChangeLanguageCommand implements ActionCommand {
    private static final String PARAM_LANGUAGE = "language";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String language = request.getParameter(PARAM_LANGUAGE);
        HttpSession session = request.getSession(true);
        session.setAttribute("language", language);
        Locale locale;
        if ("en_US".equals(language)) {
            locale = new Locale("en", "US");
        } else {
            locale = new Locale("","");
        }
        MessageManager.changeLocale(locale);
        return  ConfigurationManager.getProperty("path.page.login");
    }
}
