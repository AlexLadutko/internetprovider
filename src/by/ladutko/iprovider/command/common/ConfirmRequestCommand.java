package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.*;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.*;
import by.ladutko.iprovider.entity.*;

//TODO:TEST!!!

public class ConfirmRequestCommand implements ActionCommand {
    private static final String REQUEST_ID = "requestId";


    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try {
            String page = null;
            Integer requestID = Integer.parseInt(request.getParameter(REQUEST_ID));
            RequestService service = RequestService.getInstance();
            if (service.confirmRequest(requestID)) {
                request.setAttribute("confirmMessage", MessageManager.getProperty("message.confirmSuccessful"));
                page = ConfigurationManager.getProperty("path.page.returnToAdminRequestsList");
            } else {
                request.setAttribute("confirmMessage", MessageManager.getProperty("message.confirmFailed"));
                page = ConfigurationManager.getProperty("path.page.returnToAdminRequestsList");
            }

            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }
    }
}
