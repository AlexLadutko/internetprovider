package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.*;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.*;
import by.ladutko.iprovider.entity.*;

//TODO:TEST!!!

public class AdminUsersListCommand implements ActionCommand {
    private static final int ROWS_IN_PAGE = 5;
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try {
            String page = null;
            int currentPage = 1;
            if (request.getParameter("currentPage") != null) {
                currentPage = Integer.parseInt(request.getParameter("currentPage"));
            }
            UserService service = UserService.getInstance();
            List<User> users = new ArrayList<User>();
            int numberOfRecords = service.buildUserListDividedByPages((currentPage - 1) * ROWS_IN_PAGE, ROWS_IN_PAGE, users);
            Set<Integer> banPretenders = service.buildBanPretendersSet();
            int numberOfPages = (int) Math.ceil(numberOfRecords * 1.0 / ROWS_IN_PAGE);
            request.setAttribute("users", users);
            request.setAttribute("banPretenders", banPretenders);
            request.setAttribute("numberOfPages", numberOfPages);
            request.setAttribute("currentPage", currentPage);
            page = ConfigurationManager.getProperty("path.page.adminUsersList");

            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }
    }
}
