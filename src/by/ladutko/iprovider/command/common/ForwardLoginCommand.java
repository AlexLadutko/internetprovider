package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.*;
import by.ladutko.iprovider.entity.*;

public class ForwardLoginCommand implements ActionCommand {


    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = ConfigurationManager.getProperty("path.page.login");
        return page;
    }
}
