package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.ServiceException;
import by.ladutko.iprovider.service.UserService;
import by.ladutko.iprovider.entity.User;

public class RegistrationCommand implements ActionCommand {
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String SECOND_PASSWORD = "secondPassword";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String EMAIL = "email";
    private static final String PHONE = "phone";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try {
            String page = null;
            String login = request.getParameter(LOGIN);
            String pass = request.getParameter(PASSWORD);
            String passTwo = request.getParameter(SECOND_PASSWORD);
            String firstName = request.getParameter(FIRST_NAME);
            String lastName = request.getParameter(LAST_NAME);
            String email = request.getParameter(EMAIL);
            String phone = request.getParameter(PHONE);

            UserService service = UserService.getInstance();

            HashMap<String, String> errorMessages = service.validateUser(login, pass, passTwo, firstName, lastName, email, phone);
            if (errorMessages.isEmpty()) {
                User user = new User();
                user.setLogin(login);
                user.setPassword(pass);
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(email);
                user.setPhone(phone);
                if (service.insertUser(user)) {
                    request.setAttribute("registrationSuccessful", MessageManager.getProperty("message.registrationSuccessful"));
                    page = ConfigurationManager.getProperty("path.page.login");
                } else {
                    request.setAttribute("registrationFailed", MessageManager.getProperty("message.registrationFailed"));
                    page = ConfigurationManager.getProperty("path.page.registration");
                }
            } else {
                for (Map.Entry<String, String> entry : errorMessages.entrySet()) {
                    request.setAttribute(entry.getKey(), entry.getValue());
                }
                page = ConfigurationManager.getProperty("path.page.registration");
            }
            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }
    }
}
