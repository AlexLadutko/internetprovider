package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.*;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.*;
import by.ladutko.iprovider.entity.*;

//TODO:TEST!!!

public class ForwardChangeUserInfoCommand implements ActionCommand {
    private static final String USER_ID = "userId";

    public String execute(HttpServletRequest request) throws CommandException {
        try {
            int userId = Integer.valueOf(request.getParameter(USER_ID));
            User user = UserService.getInstance().findUserById(userId);
            request.setAttribute("user", user);
            String page = ConfigurationManager.getProperty("path.page.changeUserInfo");
            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }
    }
}
