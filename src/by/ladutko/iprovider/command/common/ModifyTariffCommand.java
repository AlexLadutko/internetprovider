package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.*;
import by.ladutko.iprovider.entity.*;

//TODO:TEST!!!

public class ModifyTariffCommand implements ActionCommand {
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String DEPRECATED = "deprecated";
    private static final String PAYMENT = "payment";
    private static final String SPEED = "speed";
    private static final String TRAFFIC = "traffic";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try {
            String page = null;
            Integer id = Integer.parseInt(request.getParameter(ID));
            String name = request.getParameter(NAME);
            Boolean deprecated = Boolean.parseBoolean(request.getParameter(DEPRECATED));
            Integer payment = Integer.parseInt(request.getParameter(PAYMENT));
            Integer speed = Integer.parseInt(request.getParameter(SPEED));
            Integer traffic = Integer.parseInt(request.getParameter(TRAFFIC));

            TariffService service = TariffService.getInstance();

            HashMap<String, String> errorMessages = service.validateTariffForUpdate(name, payment, speed, traffic);
            if (errorMessages.isEmpty()) {
                Tariff tariff = new Tariff();
                tariff.setTariffId(id);
                tariff.setName(name);
                tariff.setDeprecated(deprecated);
                tariff.setMonthPayment(payment);
                tariff.setMaxSpeed(speed);
                tariff.setTraffic(traffic);
                if (service.updateTariff(tariff)) {
                    request.setAttribute("changeMessage", MessageManager.getProperty("message.tariffUpdateSuccessful"));
                    page = ConfigurationManager.getProperty("path.page.returnToAdminTariffsList");
                } else {
                    request.setAttribute("changeMessage", MessageManager.getProperty("message.tariffUpdateFailed"));
                    page = ConfigurationManager.getProperty("path.page.returnToAdminTariffsList");
                }
            } else {
                for (Map.Entry<String, String> entry : errorMessages.entrySet()) {
                    request.setAttribute(entry.getKey(), entry.getValue());
                }
                page = ConfigurationManager.getProperty("path.page.changeTariff");
            }
            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }
    }
}
