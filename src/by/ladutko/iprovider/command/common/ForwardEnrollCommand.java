package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.*;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.*;
import by.ladutko.iprovider.entity.*;

//TODO:TEST!!!

public class ForwardEnrollCommand implements ActionCommand {
    private static final String ACCOUNT_ID = "accountId";

    public String execute(HttpServletRequest request) throws CommandException {
        int accountId = Integer.valueOf(request.getParameter(ACCOUNT_ID));
        request.setAttribute("accountId", accountId);
        String page = ConfigurationManager.getProperty("path.page.enroll");
        return page;
    }
}
