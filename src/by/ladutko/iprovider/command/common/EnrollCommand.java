package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.*;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.*;
import by.ladutko.iprovider.entity.*;

//TODO:TEST hard!!!


public class EnrollCommand implements ActionCommand  {
    private static final String ACCOUNT_ID = "accountId";
    private static final String ENROLL_SUM= "enrollSum";
    private static final String USER_ID = "sessionUserId";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try {
            String page = null;
            int userId = (Integer)request.getSession().getAttribute(USER_ID);
            int accountId = Integer.valueOf(request.getParameter(ACCOUNT_ID));
            int enrollSum = Integer.valueOf(request.getParameter(ENROLL_SUM));
            if(enrollSum <= 0){
                page = ConfigurationManager.getProperty("path.page.enroll");
                request.setAttribute("invalidSum", MessageManager.getProperty("message.invalidPayment"));
                return page;
            }
            UserService service = UserService.getInstance();

            if (service.doEnroll(accountId, enrollSum)) {
                request.setAttribute("enrollMessage", MessageManager.getProperty("message.enrollSuccessful"));
                page = ConfigurationManager.getProperty("path.page.returnToUserCabinet") + userId;
            } else {
                request.setAttribute("enrollMessage", MessageManager.getProperty("message.enrollFailed"));
                page = ConfigurationManager.getProperty("path.page.returnToUserCabinet") + userId;
            }
            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }

    }
}
