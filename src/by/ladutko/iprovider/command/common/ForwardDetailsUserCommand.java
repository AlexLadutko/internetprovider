package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.*;
import by.ladutko.iprovider.entity.*;

//TODO:TEST!!!

public class ForwardDetailsUserCommand implements ActionCommand {
    private static final String PARAM_USER_ID = "userId";

    public String execute(HttpServletRequest request) throws CommandException {
        try {
            int userId = Integer.valueOf(request.getParameter(PARAM_USER_ID));
            List<Account> accounts = UserService.getInstance().buildUserAccountList(userId);
            User user = UserService.getInstance().findUserById(userId);
            request.setAttribute("accounts", accounts );
            request.setAttribute("user", user );
            String page = ConfigurationManager.getProperty("path.page.adminUserDetails");
            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }
    }
}