package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;

import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.ConfigurationManager;

//TODO: get rid of it
public class EmptyCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = ConfigurationManager.getProperty("path.page.login");
        return page;
    }
}
