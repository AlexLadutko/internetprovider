package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.*;
import by.ladutko.iprovider.entity.*;

//TODO:TEST!!!

public class NewTariffCommand implements ActionCommand {
    private static final String NAME = "name";
    private static final String PAYMENT = "payment";
    private static final String SPEED = "speed";
    private static final String TRAFFIC = "traffic";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try {
            String page = null;
            String name = request.getParameter(NAME);
            Integer payment = Integer.parseInt(request.getParameter(PAYMENT));
            Integer speed = Integer.parseInt(request.getParameter(SPEED));
            Integer traffic = Integer.parseInt(request.getParameter(TRAFFIC));

            TariffService service = TariffService.getInstance();

            HashMap<String, String> errorMessages = service.validateTariff(name, payment, speed, traffic);
            if (errorMessages.isEmpty()) {
                Tariff tariff = new Tariff();
                tariff.setName(name);
                tariff.setDeprecated(false);
                tariff.setMonthPayment(payment);
                tariff.setMaxSpeed(speed);
                tariff.setTraffic(traffic);
                if (service.insertTariff(tariff)) {
                    request.setAttribute("creationSuccessful", MessageManager.getProperty("message.tariffCreationSuccessful"));
                    page = ConfigurationManager.getProperty("path.page.newTariff");
                } else {
                    request.setAttribute("creationFailed", MessageManager.getProperty("message.tariffCreationFailed"));
                    page = ConfigurationManager.getProperty("path.page.newTariff");
                }
            } else {
                for (Map.Entry<String, String> entry : errorMessages.entrySet()) {
                    request.setAttribute(entry.getKey(), entry.getValue());
                }
                page = ConfigurationManager.getProperty("path.page.newTariff");
            }
            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }
    }
}
