package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.ConfigurationManager;


public class LogOutCommand implements ActionCommand {

    private static final String PARAM_LANGUAGE = "language";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = ConfigurationManager.getProperty("path.page.index");
        String language = (String) request.getSession().getAttribute(PARAM_LANGUAGE);
        request.getSession().invalidate();
        HttpSession session = request.getSession(true);
        session.setAttribute("language", language);
        return page;
    }
}
