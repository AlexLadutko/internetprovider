package by.ladutko.iprovider.command.common;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import by.ladutko.iprovider.command.ActionCommand;
import by.ladutko.iprovider.command.CommandException;
import by.ladutko.iprovider.resource.*;
import by.ladutko.iprovider.service.*;
import by.ladutko.iprovider.entity.*;

//TODO:TEST!!!

public class ForwardCreateRequestCommand implements ActionCommand {
    private static final String ACCOUNT_ID = "accountId";
    private static final String PREV_TARIFF_ID = "prevTariff";

    public String execute(HttpServletRequest request) throws CommandException {
        try {
            String page = null;

            int accountId = Integer.valueOf(request.getParameter(ACCOUNT_ID ));
            int prevTariff = Integer.valueOf(request.getParameter(PREV_TARIFF_ID));

            TariffService service = TariffService.getInstance();
            Tariff currentTariff = service.findTariffById(prevTariff);
            List<Tariff> tariffs = service.buildUndeprecatedTariffList();

            request.setAttribute("accountId", accountId);
            request.setAttribute("currentTariff", currentTariff);
            request.setAttribute("tariffs", tariffs);
            page = ConfigurationManager.getProperty("path.page.newRequest");

            return page;
        }catch (ServiceException e){
            throw new CommandException(e);
        }
    }
}
