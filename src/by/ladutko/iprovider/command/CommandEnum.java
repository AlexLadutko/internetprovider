package by.ladutko.iprovider.command;

import by.ladutko.iprovider.command.common.*;

public enum CommandEnum {
    LOGIN {
        {
            this.command = new LogInCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogOutCommand();
        }
    },
    CHANGE_LANGUAGE {
        {
            this.command = new ChangeLanguageCommand();
        }
    },
    REGISTRATION {
        {
            this.command = new RegistrationCommand();
        }
    },
    NEW_TARIFF {
        {
            this.command = new NewTariffCommand();
        }
    },
    ADMIN_TARIFFS_LIST {
        {
            this.command = new AdminTariffsListCommand();
        }
    },
    DELETE_TARIFF {
        {
            this.command = new DeleteTariffCommand();
        }
    },
    FORWARD_CHANGE_TARIFF {
        {
            this.command = new ForwardChangeTariffCommand();
        }
    },
    MODIFY_TARIFF {
        {
            this.command = new ModifyTariffCommand();
        }
    },
    ADMIN_USERS_LIST {
        {
            this.command = new AdminUsersListCommand();
        }
    },
    FORWARD_DETAILS_USER {
        {
            this.command = new ForwardDetailsUserCommand();
        }
    },
    ADMIN_REQUESTS_LIST {
        {
            this.command = new AdminRequestsListCommand();
        }
    },
    CONFIRM_REQUEST {
        {
            this.command = new ConfirmRequestCommand();
        }
    },
    CABINET {
        {
            this.command = new UserCabinetCommand();
        }
    },
    FORWARD_CHANGE_USER_INFO {
        {
            this.command = new ForwardChangeUserInfoCommand();
        }
    },
    CHANGE_USER_INFO {
        {
            this.command = new ChangeUserInfoCommand();
        }
    },
    FORWARD_CHANGE_USER_PASSWORD {
        {
            this.command = new ForwardChangeUserPasswordCommand();
        }
    },
    CHANGE_USER_PASSWORD {
        {
            this.command = new ChangeUserPasswordCommand();
        }
    },
    FORWARD_CREATE_REQUEST {
        {
            this.command = new ForwardCreateRequestCommand();
        }
    },
    CREATE_REQUEST {
        {
            this.command = new CreateRequestCommand();
        }
    },
    FORWARD_ADD_ACCOUNT {
        {
            this.command = new ForwardAddAccountCommand();
        }
    },
    ADD_ACCOUNT {
        {
            this.command = new AddAccountCommand();
        }
    },
    FORWARD_ENROLL {
        {
            this.command = new ForwardEnrollCommand();
        }
    },
    ENROLL {
        {
            this.command = new EnrollCommand();
        }
    },
    FORWARD_ADMINISTRATOR {
        {
            this.command = new ForwardAdministratorCommand();
        }
    },
    FORWARD_LOGIN {
        {
            this.command = new ForwardLoginCommand();
        }
    },
    FORWARD_REGISTRATION {
        {
            this.command = new ForwardRegistrationCommand();
        }
    },
    FORWARD_NEW_TARIFF {
        {
            this.command = new ForwardNewTariffCommand();
        }
    },
    ADMIN_USER_DETAILS {
        {
            this.command = new AdminUserDetailsCommand();
        }
    };
    ActionCommand command;

    public ActionCommand getCurrentCommand() {
        return command;
    }
}
