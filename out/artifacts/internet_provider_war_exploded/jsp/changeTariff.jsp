<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customTags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<html>
<head>
    <title><fmt:message key="changeTariff.title"/></title>
</head>
<body>
<%@include file="/jsp/header.jspf" %>
<div class="row">
    <div class="medium-6 medium-centered large-4 large-centered columns">

        <form name="changeTariffForm" method="POST" action="/controller">
            <input type="hidden" name="command" value="modify_tariff"/>
            <input type="hidden" name="id" value="${ tariff.tariffId }"/>

            <div class="row column">
                <h4 class="text-center"><fmt:message key="changeTariff.header"/></h4>
                ${invalidName}
                <label><fmt:message key="changeTariff.name"/>
                    <input id="name" type="text" name="name" value="${ tariff.name }" required="true"/>
                </label>
                <c:choose>
                    <c:when test="${ tariff.isDeprecated() }">
                        <label><fmt:message key="changeTariff.deprecated"/>
                            <input checked id="deprecated" type="checkbox" name="deprecated"
                                   value="true">
                        </label>
                    </c:when>
                    <c:otherwise>
                        <label><fmt:message key="changeTariff.deprecated"/>
                            <input id="deprecated1" type="checkbox" name="deprecated"
                                   value="true">
                        </label>
                    </c:otherwise>
                </c:choose>
                ${invalidPayment}
                <label><fmt:message key="changeTariff.payment"/>
                    <input id="payment" type="text" name="payment" value="${ tariff.monthPayment }"
                           required="true"/>
                </label>
                ${invalidSpeed}
                <label><fmt:message key="changeTariff.speed"/>
                    <input id="speed" type="text" name="speed" value="${ tariff.maxSpeed }"
                           required="true"/>
                </label>
                ${invalidTraffic}
                <label><fmt:message key="changeTariff.traffic"/>
                    <input id="traffic" type="text" name="traffic" value="${ tariff.traffic }"
                           required="true"/>
                </label>

                <p><input type="submit" class="button expanded" value="<fmt:message key="changeTariff.confirm"/>"/></p>

            </div>
        </form>

    </div>
</div>
<ctg:footerTag/>
</body>
</html>