<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customTags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<html>
<head>
    <title><fmt:message key="enroll.title"/></title>
</head>
<body>
<%@include file="/jsp/header.jspf" %>
${registrationSuccessful}
<div class="row">
    <div class="medium-6 medium-centered large-4 large-centered columns">

        <form name="enrollForm" method="POST" action="/controller">
            <input type="hidden" name="command" value="enroll"/>
            <input type="hidden" name="accountId" value=${accountId}>

            <div class="row column">
                <h4 class="text-center"><fmt:message key="enroll.header"/></h4>
                ${invalidSum}
                <label><fmt:message key="enroll.cash"/>
                    <input id="enrollField" type="text" name="enrollSum" value="" required="true"
                           pattern="[0-9]*"
                           title="<fmt:message key="enroll.invalidCash"/>"/>
                </label>

                <p><input type="submit" class="button expanded" value="<fmt:message key="userCabinet.enroll"/>"/></p>

            </div>
        </form>

    </div>
</div>
<ctg:footerTag/>
</body>
</html>