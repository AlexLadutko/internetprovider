<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customTags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<html>
<head>
    <title><fmt:message key="adminTariffList.title"/></title>
</head>
<body>
<%@include file="/jsp/header.jspf" %>
${changeMessage}${deleteMessage}
<h4 class="text-center"><fmt:message key="adminTariffList.header"/></h4>
<table class="hover" border="1">
    <thead>
    <tr>
        <th><fmt:message key="adminTariffList.id"/></th>
        <th><fmt:message key="adminTariffList.tariffName"/></th>
        <th><fmt:message key="adminTariffList.isDeprecated"/></th>
        <th><fmt:message key="adminTariffList.payment"/></th>
        <th><fmt:message key="adminTariffList.speed"/></th>
        <th><fmt:message key="adminTariffList.traffic"/></th>
        <th><fmt:message key="adminTariffList.change"/></th>
        <th><fmt:message key="adminTariffList.delete"/></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="tariff" items="${tariffs}">
        <tr>
            <td><c:out value="${ tariff.tariffId }"/></td>
            <td><c:out value="${ tariff.name }"/></td>
            <td><c:out value="${ tariff.isDeprecated() }"/></td>
            <td><c:out value="${ tariff.monthPayment }"/></td>
            <td><c:out value="${ tariff.maxSpeed }"/></td>
            <td><c:out value="${ tariff.traffic }"/></td>
            <td>
                <form name="changeTariff" method="POST" action="/controller">
                    <input type="hidden" name="command" value="forward_change_tariff">
                    <input type="hidden" name="tariffId" value=${tariff.tariffId}>
                    <input type="submit" value=<fmt:message key="adminTariffList.change"/>>
                </form>
            </td>
            <td>
                <form name="deleteTariff" method="POST" action="/controller"
                      onsubmit="return confirm('<fmt:message key="adminTariffList.confirmDelete"/>')">
                    <input type="hidden" name="command" value="delete_tariff">
                    <input type="hidden" name="tariffId" value=${tariff.tariffId}>
                    <input type="submit" value="<fmt:message key="adminTariffList.delete"/>"/>
                </form>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<ul class="pagination text-center" role="navigation" aria-label="Pagination">
    <c:if test="${currentPage != 1}">
        <li class="pagination-previous">
            <a href="/controller?command=admin_tariffs_list&currentPage=${currentPage - 1}">
                <fmt:message key="adminTariffList.previous"/>
            </a>
        </li>
    </c:if>

    <c:forEach begin="1" end="${numberOfPages}" var="counter">
        <c:choose>
            <c:when test="${currentPage eq counter}">
                <li class="current">${counter}</li>
            </c:when>
            <c:otherwise>
                <li>
                    <a href="/controller?command=admin_tariffs_list&currentPage=${counter}">
                            ${counter}
                    </a>
                </li>
            </c:otherwise>
        </c:choose>
    </c:forEach>

    <c:if test="${currentPage lt numberOfPages}">
        <li class="pagination-next">
            <a href="/controller?command=admin_tariffs_list&currentPage=${currentPage + 1}">
                <fmt:message key="adminTariffList.next"/>
            </a>
        </li>
    </c:if>
</ul>
<ctg:footerTag/>
</body>
</html>