<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customTags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<html>
<head>
    <title><fmt:message key="changeUserInfo.title"/></title>
</head>
<body>
<%@include file="/jsp/header.jspf" %>
${changeUserFailed}
<div class="row">
    <div class="medium-6 medium-centered large-4 large-centered columns">

        <form name="changeUserInfoForm" method="POST" action="/controller">
            <input type="hidden" name="command" value="change_user_info"/>
            <input type="hidden" name="userId" value="${sessionScope.sessionUserId}"/>

            <div class="row column">
                <h4 class="text-center"><fmt:message key="changeUserInfo.header"/></h4>
                ${invalidLogin}
                <label><fmt:message key="changeUserInfo.login"/>
                    <input id="login" type="text" name="login" value="${user.login}" required pattern=".{6,}"
                           title="<fmt:message key="changeUserInfo.invalidLogin"/>"/>
                </label>
                ${invalidFirstName}
                <label><fmt:message key="changeUserInfo.firstName"/>
                    <input id="firstName" type="text" name="firstName" value="${user.firstName}" required pattern="[A-Za-z]{2,}"
                           title="<fmt:message key="changeUserInfo.invalidName"/>"/>
                </label>
                ${invalidLastName}
                <label><fmt:message key="changeUserInfo.lastName"/>
                    <input id="lastName" type="text" name="lastName" value="${user.lastName}" required pattern="[A-Za-z]{2,}"
                           title="<fmt:message key="changeUserInfo.invalidName"/>"/>
                </label>
                ${invalidEmail}
                <label><fmt:message key="changeUserInfo.email"/>
                    <input id="email" type="email" name="email" value="${user.email}" required
                           pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
                           title="<fmt:message key="changeUserInfo.invalidEmail"/>"/>
                </label>

                ${invalidPhone}
                <label><fmt:message key="changeUserInfo.phone"/>
                    <input id="phone" type="text" name="phone" value="${user.phone}" required pattern="[0-9]{9,}"
                           title="<fmt:message key="changeUserInfo.invalidPhone"/>"/>
                </label>

                <p><input type="submit" class="button expanded" value="<fmt:message key="changeUserInfo.change"/>"/></p>

            </div>
        </form>

    </div>
</div>
<ctg:footerTag/>
</body>
</html>