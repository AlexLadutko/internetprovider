<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customTags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<html>
<head>
    <title><fmt:message key="userCabinet.title"/></title>
</head>
<body>
<%@include file="/jsp/header.jspf" %>
${changeUserSuccessful}
${changePasswordSuccessful}
${createRequestSuccessful}
${createRequestFailed}
${addAccountMessage}
${enrollMessage}
<h4 class="text-center"><fmt:message key="userCabinet.header"/></h4>
<table class="hover">
    <tr>
        <td><fmt:message key="userCabinet.id"/></td>
        <td><c:out value="${ user.userId }"/></td>
    </tr>
    <tr>
        <td><fmt:message key="userCabinet.login"/></td>
        <td><c:out value="${ user.login }"/></td>
    </tr>
    <tr>
        <td><fmt:message key="userCabinet.firstName"/></td>
        <td><c:out value="${ user.firstName }"/></td>
    </tr>
    <tr>
        <td><fmt:message key="userCabinet.lastName"/></td>
        <td><c:out value="${ user.lastName }"/></td>
    </tr>
    <tr>
        <td><fmt:message key="userCabinet.email"/></td>
        <td><c:out value="${ user.email}"/></td>
    </tr>
    <tr>
        <td><fmt:message key="userCabinet.phone"/></td>
        <td><c:out value="${ user.phone }"/></td>
    </tr>
</table>
<br/><br/><br/>

<form name="forwardChangeUserInfo" id="forwardChangeUserInfo" method="POST" action="/controller">
    <input type="hidden" name="command" value="forward_change_user_info"/>
    <input type="hidden" name="userId" value="${sessionScope.sessionUserId}"/>
</form>
<form name="forwardChangeUserPassword" id="forwardChangeUserPassword" method="POST" action="/controller">
    <input type="hidden" name="command" value="forward_change_user_password"/>
    <input type="hidden" name="userId" value="${sessionScope.sessionUserId}"/>
</form>
<form name="forwardAddAccountForm" id="forwardAddAccountForm" method="POST" action="/controller">
    <input type="hidden" name="command" value="forward_add_account"/>
    <input type="hidden" name="userId" value="${sessionScope.sessionUserId}"/>
</form>

<div class="expanded button-group">
    <input type="submit" class="button" form="forwardChangeUserInfo"
           value="<fmt:message key="userCabinet.changeUserInfo"/>"/>
    <input type="submit" class="button" form="forwardChangeUserPassword"
           value="<fmt:message key="userCabinet.changeUserPassword"/>"/>
    <input type="submit" class="button" form="forwardAddAccountForm"
           value="<fmt:message key="userCabinet.addAccount"/>"/>
</div>
<br/><br/><br/>
<table border="1">
    <caption><fmt:message key="userCabinet.caption"/></caption>
    <thead>
    <tr>
        <th><fmt:message key="userCabinet.accountId"/></th>
        <th><fmt:message key="userCabinet.tariffId"/></th>
        <th><fmt:message key="userCabinet.balance"/></th>
        <th><fmt:message key="userCabinet.creationDate"/></th>
        <th><fmt:message key="userCabinet.ban"/></th>
        <th><fmt:message key="userCabinet.changeTariff"/></th>
        <th><fmt:message key="userCabinet.enroll"/></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="account" items="${accounts}">
        <tr>
            <td><c:out value="${ account.accountId }"/></td>
            <td><c:out value="${ account.tariffId }"/></td>
            <td><c:out value="${ account.balance }"/></td>
            <td><c:out value="${ account.creationDate }"/></td>
            <td><c:out value="${ account.ban }"/></td>
            <c:choose>
                <c:when test="${ account.ban == 'true' }">
                    <td>
                        <form name="changeTariffUser" method="POST" action="/controller">
                            <input type="hidden" name="command" value="forward_create_request">
                            <input type="hidden" name="accountId" value=${account.accountId}>
                            <input type="hidden" name="prevTariff" value=${account.tariffId}>
                            <input disabled type="submit" value=<fmt:message key="userCabinet.changeTariff"/>>
                        </form>
                    </td>
                </c:when>
                <c:otherwise>
                    <td>
                        <form name="changeTariffUser" method="POST" action="/controller">
                            <input type="hidden" name="command" value="forward_create_request">
                            <input type="hidden" name="accountId" value=${account.accountId}>
                            <input type="hidden" name="prevTariff" value=${account.tariffId}>
                            <input type="submit" value=<fmt:message key="userCabinet.changeTariff"/>>
                        </form>
                    </td>
                </c:otherwise>
            </c:choose>
            <td>
                <form name="forwardEnrollUser" method="POST" action="/controller">
                    <input type="hidden" name="command" value="forward_enroll">
                    <input type="hidden" name="accountId" value=${account.accountId}>
                    <input type="submit" value=<fmt:message key="userCabinet.enroll"/>>
                </form>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<ctg:footerTag/>
</body>
</html>