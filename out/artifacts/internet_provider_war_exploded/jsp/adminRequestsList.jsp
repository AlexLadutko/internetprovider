<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customTags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<html>
<head>
    <title><fmt:message key="adminRequestList.title"/></title>
</head>
<body>
<%@include file="/jsp/header.jspf" %>
${confirmMessage}
<h4 class="text-center"><fmt:message key="adminRequestList.header"/></h4>
<table class="hover" border="1">
    <thead>
    <tr>
        <th><fmt:message key="adminRequestList.requestId"/></th>
        <th><fmt:message key="adminRequestList.accountId"/></th>
        <th><fmt:message key="adminRequestList.prevTariffId"/></th>
        <th><fmt:message key="adminRequestList.newTariffId"/></th>
        <th><fmt:message key="adminRequestList.requestDate"/></th>
        <th><fmt:message key="adminRequestList.confirm"/></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="request" items="${requests}">
        <tr>
            <td><c:out value="${ request.requestId }"/></td>
            <td><c:out value="${ request.accountId }"/></td>
            <td><c:out value="${ request.prevTariffId }"/></td>
            <td><c:out value="${ request.newTariffId }"/></td>
            <td><c:out value="${ request.requestDate}"/></td>
            <td>
                <form name="confirmRequestForm" method="POST" action="/controller">
                    <input type="hidden" name="command" value="confirm_request">
                    <input type="hidden" name="requestId" value=${request.requestId}>
                    <input type="submit" value=<fmt:message key="adminRequestList.confirm"/>>
                </form>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<ul class="pagination text-center" role="navigation" aria-label="Pagination">
    <c:if test="${currentPage != 1}">
        <li class="pagination-previous">
            <a href="/controller?command=admin_requests_list&currentPage=${currentPage - 1}">
                <fmt:message key="adminRequestList.previous"/>
            </a>
        </li>
    </c:if>

    <c:forEach begin="1" end="${numberOfPages}" var="counter">
        <c:choose>
            <c:when test="${currentPage eq counter}">
                <li class="current">${counter}</li>
            </c:when>
            <c:otherwise>
                <li>
                    <a href="/controller?command=admin_requests_list&currentPage=${counter}">
                            ${counter}
                    </a>
                </li>
            </c:otherwise>
        </c:choose>
    </c:forEach>

    <c:if test="${currentPage lt numberOfPages}">
        <li class="pagination-next">
            <a href="/controller?command=admin_requests_list&currentPage=${currentPage + 1}">
                <fmt:message key="adminRequestList.next"/>
            </a>
        </li>
    </c:if>
</ul>
<ctg:footerTag/>
</body>
</html>