<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customTags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<html>
<head>
    <title><fmt:message key="registration.title"/></title>
</head>
<body >
<%@include file="/jsp/header.jspf" %>
${registrationFailed}
<div class="row">
    <div class="medium-6 medium-centered large-4 large-centered columns">

        <form name="registrationForm" method="POST" action="/controller">
            <input type="hidden" name="command" value="registration"/>
            <div class="row column">
                <h4 class="text-center"><fmt:message key="registration.header"/></h4>
                ${invalidLogin}
                <label><fmt:message key="registration.login"/>
                    <input type="text" name="login" value="" required pattern=".{6,}"
                           title="<fmt:message key="registration.invalidLogin"/>"/>
                </label>
                ${invalidPassword}
                <label><fmt:message key="registration.password"/>
                    <input type="password" name="password" value="" required pattern=".{6,}"
                           title="<fmt:message key="registration.invalidPassword"/>"/>
                </label>
                ${invalidSecondPassword}
                <label><fmt:message key="registration.secondPassword"/>
                    <input type="password" name="secondPassword" value="" required pattern=".{6,}"
                           title="<fmt:message key="registration.invalidPassword"/>"/>
                </label>
                ${invalidFirstName}
                <label><fmt:message key="registration.firstName"/>
                    <input type="text" name="firstName" value="" required pattern="[A-Za-z]{2,}"
                           title="<fmt:message key="registration.invalidName"/>"/>
                </label>
                ${invalidLastName}
                <label><fmt:message key="registration.lastName"/>
                    <input type="text" name="lastName" value="" required pattern="[A-Za-z]{2,}"
                           title="<fmt:message key="registration.invalidName"/>"/>
                </label>
                ${invalidEmail}
                <label><fmt:message key="registration.email"/>
                    <input type="email" name="email" value="" required
                           pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
                           title="<fmt:message key="registration.invalidEmail"/>"/>
                </label>
                ${invalidPhone}
                <label><fmt:message key="registration.phone"/>
                    <input type="text" name="phone" value="" required pattern="[0-9]{9,}"
                           title="<fmt:message key="registration.invalidPhone"/>"/>
                </label>

                <p><input type="submit" class="button expanded" value="<fmt:message key="registration.register"/>"/></p>

            </div>
        </form>

    </div>
</div>
<ctg:footerTag/>
</body>
</html>